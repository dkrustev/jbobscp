;; Author: Dimitur Krustev
;; started: 20160327 (based on Coq prototype started 20160313)

;(#%require racket/trace)

;(load "../j-bob/scheme/j-bob-lang.scm")
;(load "../j-bob/scheme/j-bob.scm")
;(load "j-bob-rewriter2.scm")

(defun append (xs ys)
  (if (atom xs) ys
      (cons (car xs) (append (cdr xs) ys))))

(defun last (xs)
  (if (atom xs) 'nil
      (if (atom (cdr xs)) (car xs)
          (last (cdr xs)))))

(defun butlast (xs)
  (if (atom xs) 'nil
      (if (atom (cdr xs)) '()
          (cons (car xs) (butlast (cdr xs))))))

; `opt-plus` actually returns the first non-empty list, if any
(defun opt-plus (o1 o2)
  (if (atom o1) o2 o1))

;;;

(defun if-lifting-steps (path pathsuffix exp q e a)
  (list3
   (list2 path (app-c 'if-same (list2 q exp)))
   (list2 (append path (cons 'A pathsuffix)) (app-c 'if-nest-A (list3 q e a)))
   (list2 (append path (cons 'E pathsuffix)) (app-c 'if-nest-E (list3 q e a)))
   ))

;(defun if-reducible? (eroot path q)
;  (if (equal q ''t) 't
;      (if (equal q ''nil) 't
;          (if (prem-A?2 q path eroot) 't
;              (prem-E?2 q path eroot)))))

(defun context-good-for-if-lifting (dir)
  (if (natp dir)
      't
      (equal dir 'Q)))

(defun try-if-lifting (eroot path dir q a e)
  (if (context-good-for-if-lifting dir)
      (if-lifting-steps path (list1 dir) (find-focus-at-path2 path eroot) q a e)
      '()))

(defun simplify-if (fullscp eroot path q a e)
  (if (equal q ''t)
      (list1 (list2 path (app-c 'if-true (list2 a e))))
      (if (equal q ''nil)
          (list1 (list2 path (app-c 'if-false (list2 a e))))
          (if fullscp
              (if (prem-A?2 q path eroot)
                  (list1 (list2 path (app-c 'if-nest-A (list3 q a e))))
                  (if (prem-E?2 q path eroot)
                      (list1 (list2 path (app-c 'if-nest-E (list3 q a e))))
                      (if (equal a e)
                          (list1 (list2 path (app-c 'if-same (list2 q a))))
                          (if (atom path)
                              '()
                              (try-if-lifting eroot (butlast path) (last path) q a e)))))
              '()))))

(defun simplify-atom (path arg)
  (if (app? arg)
      (if (list2? (app.args arg))
          (if (equal (app.name arg) 'cons)
              (list1 (list2 path (app-c 'atom/cons (app.args arg))))
              '())
          '())
      '()))

(defun simplify-car (path arg)
  (if (app? arg)
      (if (list2? (app.args arg))
          (if (equal (app.name arg) 'cons)
              (list1 (list2 path (app-c 'car/cons (app.args arg))))
              '())
          '())
      '()))

(defun simplify-cdr (path arg)
  (if (app? arg)
      (if (list2? (app.args arg))
          (if (equal (app.name arg) 'cons)
              (list1 (list2 path (app-c 'cdr/cons (app.args arg))))
              '())
          '())
      '()))

(defun simplify-equal (path arg1 arg2)
  (if (equal arg1 arg2)
      (list1 (list2 path (app-c 'equal-same (list1 arg1))))
      '()))

(defun simplify-natp (path arg)
  (if (app? arg)
      (if (list1? (app.args arg))
          (if (equal (app.name arg) 'size)
              (list1 (list2 path (app-c 'natp/size (app.args arg))))
              '())
          '())
      '()))

(defun simplify-app-not-quoted (path name args)
  (if (list1? args)
      (if (equal name 'atom)
          (simplify-atom path (elem1 args))
          (if (equal name 'car)
              (simplify-car path (elem1 args))
              (if (equal name 'cdr)
                  (simplify-cdr path (elem1 args))
                  (if (equal name 'natp)
                      (simplify-natp path (elem1 args))
                      '()))))
      (if (list2? args)
          (if (equal name 'equal)
              (simplify-equal path (elem1 args) (elem2 args))
              '())
          '())))

(defun simplify-app (path name args)
  (if (rator? name)
      (if (quoted-exprs? args)
          (list1 (list2 path (app-c name args)))
          (simplify-app-not-quoted path name args))
      (simplify-app-not-quoted path name args)))

(defun simplify-current (fullscp eroot path e)
  (if (if? e)
      (simplify-if fullscp eroot path (if.Q e) (if.A e) (if.E e))
      (if (app? e)
          (simplify-app path (app.name e) (app.args e))
          '())))

;;;

(defun extend-path (path addpos pos)
  (if addpos
      (append path (list1 pos))
      path))

(defun simplify-es (fullscp eroot path addpos pos es)
  (if (atom es)
      '()
      (opt-plus
       (if (if? (car es))
           (opt-plus
            (simplify-es fullscp eroot (append (extend-path path addpos pos) '(Q)) 'nil '1 (list1 (if.Q (car es))))
            (opt-plus
             (simplify-es fullscp eroot (append (extend-path path addpos pos) '(A)) 'nil '1 (list1 (if.A (car es))))
             (opt-plus
              (simplify-es fullscp eroot (append (extend-path path addpos pos) '(E)) 'nil '1 (list1 (if.E (car es))))
              (simplify-es fullscp eroot path addpos (+ '1 pos) (cdr es)))))
           (if (app? (car es))
               (opt-plus
                (simplify-es fullscp eroot (extend-path path addpos pos) 't '1 (app.args (car es)))
                (simplify-es fullscp eroot path addpos (+ '1 pos) (cdr es)))
               (simplify-es fullscp eroot path addpos (+ '1 pos) (cdr es))))
       (simplify-current fullscp eroot (extend-path path addpos pos) (car es))
       )))

(defun simplify-e (fullscp eroot path e)
  (simplify-es fullscp eroot path 'nil '1 (list1 e)))

(defun simplify-top (fullscp e)
  (simplify-e fullscp e '() e))

;;;

(defun rewrite-opt-steps (defs e optsteps)
  (if (atom optsteps)
      '()
      (cons optsteps (cdr (rewrite/steps2 defs e optsteps)))))

(defun simplify*-rec (defs fullscp fuel prev)
  (if (atom prev)
      '()
      (if (atom fuel)
          '()
          (append (car prev)
                  (simplify*-rec defs fullscp (cdr fuel)
                                 (rewrite-opt-steps defs (cdr prev)
                                                    (simplify-top fullscp (cdr prev))))))))

(defun simplify* (defs fullscp fuel e)
  (simplify*-rec defs fullscp fuel (rewrite-opt-steps defs e (simplify-top fullscp e))))

;;;

(defun unfold-step-current (defs path e)
  (if (app? e)
      (if (defun? (lookup (app.name e) defs))
          (list1 (list2 path e))
          '())
      '()))

(defun unfold-steps-es (defs path addpos pos es)
  (if (atom es)
      '()
      (append
       (unfold-step-current defs (extend-path path addpos pos) (car es))
       (if (if? (car es))
           (append
            (unfold-steps-es defs (append (extend-path path addpos pos) '(Q)) 'nil '1 (list1 (if.Q (car es))))
            (append
             (unfold-steps-es defs (append (extend-path path addpos pos) '(A)) 'nil '1 (list1 (if.A (car es))))
             (append
              (unfold-steps-es defs (append (extend-path path addpos pos) '(E)) 'nil '1 (list1 (if.E (car es))))
              (unfold-steps-es defs path addpos (+ '1 pos) (cdr es)))))
           (if (app? (car es))
               (append
                (unfold-steps-es defs (extend-path path addpos pos) 't '1 (app.args (car es)))
                (unfold-steps-es defs path addpos (+ '1 pos) (cdr es)))
               (unfold-steps-es defs path addpos (+ '1 pos) (cdr es)))))))

(defun unfold-steps-e (defs path e)
  (unfold-steps-es defs path 'nil '1 (list1 e)))

(defun unfold-steps-top (defs e)
  (unfold-steps-e defs '() e))

;;;

(defun unfold-steps-cbv-es (defs path addpos pos es)
  (if (atom es)
      '()
      (append
       (if (if? (car es))
           (append
            (unfold-steps-cbv-es defs (append (extend-path path addpos pos) '(Q)) 'nil '1 (list1 (if.Q (car es))))
            (append
             (unfold-steps-cbv-es defs (append (extend-path path addpos pos) '(A)) 'nil '1 (list1 (if.A (car es))))
             (append
              (unfold-steps-cbv-es defs (append (extend-path path addpos pos) '(E)) 'nil '1 (list1 (if.E (car es))))
              (unfold-steps-cbv-es defs path addpos (+ '1 pos) (cdr es)))))
           (if (app? (car es))
               (append
                (unfold-steps-cbv-es defs (extend-path path addpos pos) 't '1 (app.args (car es)))
                (unfold-steps-cbv-es defs path addpos (+ '1 pos) (cdr es)))
               (unfold-steps-cbv-es defs path addpos (+ '1 pos) (cdr es))))
       (unfold-step-current defs (extend-path path addpos pos) (car es))
       )))

(defun unfold-steps-cbv-e (defs path e)
  (unfold-steps-cbv-es defs path 'nil '1 (list1 e)))

(defun unfold-steps-cbv-top (defs e)
  (unfold-steps-cbv-e defs '() e))

;;;

(defun apply-simplify (defs fullscp simplfuel e)
  (cdr (rewrite/steps2 defs e (simplify* defs fullscp simplfuel e))))

(defun best-unfold-step-of-pair (beststep_size newstep newsize)
  (if (< newsize (cdr beststep_size))
      (cons newstep newsize)
      beststep_size))

(defun best-unfold-step-rec (defs fullscp simplfuel e beststep_size steps)
  (if (atom steps)
      (list1 (car beststep_size))
      (best-unfold-step-rec defs fullscp simplfuel e
                            (best-unfold-step-of-pair beststep_size
                                                      (car steps)
                                                      (size (apply-simplify defs fullscp simplfuel (cdr (rewrite/step2 defs e (car steps))))))
                            (cdr steps))))

(defun best-unfold-step-among (defs fullscp simplfuel e steps)
  (if (atom steps)
      '()
      (best-unfold-step-rec defs fullscp simplfuel e
                            (cons (car steps)
                                  (size (apply-simplify defs fullscp simplfuel (cdr (rewrite/step2 defs e (car steps))))))
                            (cdr steps))))

(defun best-unfold-step (defs fullscp simplfuel e)
  (best-unfold-step-among defs fullscp simplfuel e (unfold-steps-top defs e)))

(defun unfold-step-in-whitelist (whitelist steps)
  (if (atom steps)
      '()
      (if (member? (car (elem2 (car steps))) whitelist)
          (list1 (car steps))
          (unfold-step-in-whitelist whitelist (cdr steps)))))

(defun unfold-step-not-in-blacklist (blacklist steps)
  (if (atom steps)
      '()
      (if (member? (car (elem2 (car steps))) blacklist)
          (unfold-step-not-in-blacklist blacklist (cdr steps))
          (list1 (car steps)))))

(defun choose-unfold-step (defs fullscp whitelist blacklist simplfuel e)
  (if (atom whitelist)
      (if (atom blacklist)
          (best-unfold-step defs fullscp simplfuel e)
          (unfold-step-not-in-blacklist blacklist (unfold-steps-cbv-top defs e)))
      (unfold-step-in-whitelist whitelist (unfold-steps-top defs e))))

;;;

; what it could be with let-expressions:
;(defun scp-steps (defs unfoldfuel simplfuel e)
;  (if (atom unfoldfuel)
;      (simplify* defs simplfuel e)
;      (let* ((simplsteps (simplify* defs simplfuel e))
;             (e1 (rewrite/steps defs e simplsteps))
;             (optunfoldstep (best-unfold-step defs simplfuel e1)))
;        (if (atom optunfoldstep)
;            simplsteps
;            (append simplsteps
;                    (cons (car optunfoldstep)
;                          (scp-steps defs (cdr unfoldfuel) simplfuel (rewrite/step defs e1 (car optunfoldstep)))))))))

(defun exp-with-simplsteps (defs fullscp simplfuel e)
  (cons e (simplify* defs fullscp simplfuel e)))

(defun steps-with-result (defs e_steps)
  (cons (cdr e_steps) (cdr (rewrite/steps2 defs (car e_steps) (cdr e_steps)))))

(defun optunfold-with-prevsteps-result (defs fullscp whitelist blacklist simplfuel prevsteps_e)
  (list3 (car prevsteps_e) (cdr prevsteps_e) (choose-unfold-step defs fullscp whitelist blacklist simplfuel (cdr prevsteps_e))))

(defun scp-steps-rec (defs fullscp unfoldfuel whitelist blacklist simplfuel simplsteps_e_optunfoldstep)
  (if (atom unfoldfuel)
      (elem1 simplsteps_e_optunfoldstep)
      (if (atom (elem3 simplsteps_e_optunfoldstep))
          (elem1 simplsteps_e_optunfoldstep)
          (append (elem1 simplsteps_e_optunfoldstep)
                  (cons (car (elem3 simplsteps_e_optunfoldstep))
                        (scp-steps-rec
                         defs fullscp (cdr unfoldfuel) whitelist blacklist simplfuel
                         (optunfold-with-prevsteps-result
                          defs fullscp whitelist blacklist simplfuel
                          (steps-with-result
                           defs 
                           (exp-with-simplsteps
                            defs fullscp simplfuel
                            (cdr (rewrite/step2 defs (elem2 simplsteps_e_optunfoldstep)
                                                (car (elem3 simplsteps_e_optunfoldstep)))))))))))))

(defun scp-steps (defs fullscp unfoldfuel whitelist blacklist simplfuel e)
  (scp-steps-rec defs fullscp unfoldfuel whitelist blacklist simplfuel
                 (optunfold-with-prevsteps-result
                  defs fullscp whitelist blacklist simplfuel
                  (steps-with-result defs (exp-with-simplsteps defs fullscp simplfuel e)))))

;;;

;(trace simplify-es)

;(simplify-top '(if x (f '1 (if 't '2 '3)) y))
;(simplify-top '(f a b (+ '1 '2)))
;(simplify-top '(id (if (atom x) x (cons (car x) (cdr x)))))
;
;(simplify* (axioms) '(a a a) '(if x '1 (if y '1 '1)))
;(simplify* (append (axioms) '((defun id (x) (if (atom x) x (cons (car x) (cdr x)))))) '(a a a a a a a a a a) '(if (atom x) (id (id x)) (id (id x))))
;(simplify* (append (axioms) '((defun id (x) (if (atom x) x (cons (car x) (cdr x)))))) '(a a a a a a a a a a) '(id (if (atom x) x (cons (car x) (cdr x)))))
;(simplify* (axioms) '(a a a a a a) '(if (if x y z) a b))
;
;(unfold-steps-top '((defun f (x) x) (defun g (x y) x)) '(g (if x (f '1) (g '8 '9)) (g (f (g '2 '3)) 'a)))
;(best-unfold-step '((defun f (x) x) (defun g (x y) x)) '(a a a a a a a a a a) '(g (if x (f '1) (g '8 '9)) (g (f (g '2 '3)) 'a)))
;(best-unfold-step '((defun id (x) (if (atom x) x (cons (car x) (cdr x))))) '(a a a a a a a a a a) '(if (atom x) (id (id x)) (id (id x))))
;(choose-unfold-step '((defun id (x) (if (atom x) x (cons (car x) (cdr x))))) '() '() '(a a a a a a a a a a) '(if (atom x) (id (id x)) (id (id x))))
;(choose-unfold-step '((defun id (x) (if (atom x) x (cons (car x) (cdr x))))) '(id) '() '(a a a a a a a a a a) '(if (atom x) (id (id x)) (id (id x))))
;(choose-unfold-step '((defun id (x) (if (atom x) x (cons (car x) (cdr x))))) '() '(id) '(a a a a a a a a a a) '(if (atom x) (id (id x)) (id (id x))))
;(choose-unfold-step '((defun id (x) (if (atom x) x (cons (car x) (cdr x))))) '() '(id2) '(a a a a a a a a a a) '(if (atom x) (id (id x)) (id (id x))))
;
;(scp-steps (append (axioms) '((defun id (x) (if (atom x) x (cons (car x) (cdr x)))))) '(a a a) '() '() '(a a a a a a a a a a) '(if (atom x) (id (id x)) (id (id x))))
;(rewrite/steps2
; (append (axioms) '((defun id (x) (if (atom x) x (cons (car x) (cdr x))))))
; '(if (atom x) (id (id x)) (id (id x)))
; (scp-steps (append (axioms) '((defun id (x) (if (atom x) x (cons (car x) (cdr x)))))) '(a a a) '() '() '(a a a a a a a a a a) '(if (atom x) (id (id x)) (id (id x)))))
;(scp-steps (append (axioms) '((defun id (x) (if (atom x) x (cons (car x) (cdr x)))))) '(a a a a a a) '(id) '() '(a a a a a a a a a a) '(if (atom x) (id (id x)) (id (id x))))
;(scp-steps (append (axioms) '((defun member? (x ys) (if (atom ys) 'nil (if (equal x (car ys)) 't (member? x (cdr ys)))))))
;           '(a a a a a a a a a a a a) '() '(dummy) '(a a a a a a a a a a) '(member? 'f '(a b c d e f)))
;(scp-steps (append (axioms) '((defun append (xs ys) (if (atom xs) ys (cons (car xs) (append (cdr xs) ys))))))
;           't '(a a a a) '() '() '(a a a a a a a a a a)
;           '(if (atom xs) (equal (append (append xs ys) zs) (append xs (append ys zs))) (if (equal (append (append (cdr xs) ys) zs) (append (cdr xs) (append ys zs))) (equal (append (append xs ys) zs) (append xs (append ys zs))) 't)))
;
;(defun take (n xs)
;  (if (equal n 0)
;      '()
;      (if (atom xs)
;          xs
;          (cons (car xs) (take (- n 1) (cdr xs))))))
;
;(defun drop (n xs)
;  (if (equal n 0)
;      xs
;      (if (atom xs)
;          xs
;          (drop (- n 1) (cdr xs)))))
;
;(let*
;    ((n 30)
;     (e 
;      '(if (car b_e1)
;           (if (car b_e2)
;               (if (atom path)
;                   (if (car b_e1) (if (car b_e2) (if (focus-is-at-path?2 path (cdr b_e1)) (equal (car (rewrite-focus-at-path2 path b_e1 b_e2)) 't) 't) 't) 't)
;                   (if (if (car (find-focus-at-direction2 (car path) b_e1))
;                           (if (car b_e2)
;                               (if (focus-is-at-path?2 (cdr path) (cdr (find-focus-at-direction2 (car path) b_e1)))
;                                   (equal (car (rewrite-focus-at-path2 (cdr path) (find-focus-at-direction2 (car path) b_e1) b_e2)) 't)
;                                   't)
;                               't)
;                           't)
;                       (if (car b_e1) (if (car b_e2) (if (focus-is-at-path?2 path (cdr b_e1)) (equal (car (rewrite-focus-at-path2 path b_e1 b_e2)) 't) 't) 't) 't)
;                       't))
;               (if (car b_e1) (if (car b_e2) (if (focus-is-at-path?2 path (cdr b_e1)) (equal (car (rewrite-focus-at-path2 path b_e1 b_e2)) 't) 't) 't) 't))
;           (if (car b_e1) (if (car b_e2) (if (focus-is-at-path?2 path (cdr b_e1)) (equal (car (rewrite-focus-at-path2 path b_e1 b_e2)) 't) 't) 't) 't)))
;     (steps (scp-steps (axioms) '() '() '() '(a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a) e))
;     (steps1 (take n steps))
;     (steps2 (drop n steps))
;     (e1 (rewrite/steps2 (axioms) e steps1))
;     )
;  (list3 steps1 e1 steps2))

