
;; Author: Dimitur Krustev
;; started: 20160327 (based on Coq prototype started 20160313)

;(load "../j-bob/scheme/j-bob-lang.scm")
;(load "../j-bob/scheme/j-bob.scm")
;(load "j-bob-rewriter2.scm")
;(load "j-bob-scp.scm")


(defun replicate (n x)
  (if (equal n '0)
      '()
      (cons x (replicate (- n '1) x))))

;;;

(defun find-nth-call-from (fname from n unfoldsteps)
  (if (atom unfoldsteps)
      '()
      (if (equal fname (car (elem2 (car unfoldsteps))))
          (if (equal from n)
              (elem1 (car unfoldsteps))
              (find-nth-call-from fname (+ '1 from) n (cdr unfoldsteps)))
          (find-nth-call-from fname from n (cdr unfoldsteps)))))

(defun find-nth-call (defs fname n e)
  (find-nth-call-from fname '1 n (unfold-steps-top defs e)))

(defun expand-path-rec-helper (path e)
  (cons path (find-focus-at-path2 path e)))

(defun expand-path-rec (defs path prevpath_e)
  (if (atom path)
      (car prevpath_e)
      (if (atom (car path))
          (append (car prevpath_e) (expand-path-rec defs (cdr path) (expand-path-rec-helper (list1 (car path)) (cdr prevpath_e))))
          (append (car prevpath_e)
                  (expand-path-rec defs (cdr path)
                                   (expand-path-rec-helper
                                    (find-nth-call defs (elem1 (car path)) (elem2 (car path)) (cdr prevpath_e))
                                    (cdr prevpath_e)))))))

(defun expand-path (defs path e)
  (expand-path-rec defs path (cons '() e)))

;;;

(defun expand-scp-step (defs fullscp stepargs claim)
  (if (atom stepargs)
      (scp-steps defs fullscp '() '() '() (replicate 10 'a) claim)
      (if (atom (cdr stepargs))
          (scp-steps defs fullscp (replicate (car stepargs) 'a) '() '() (replicate 10 'a) claim)
          (if (atom (cdr (cdr stepargs)))
              (scp-steps defs fullscp (replicate (car stepargs) 'a) '() '() (replicate (car (cdr stepargs)) 'a) claim)
              (if (atom (cdr (cdr (cdr stepargs))))
                  (scp-steps defs fullscp (replicate (car stepargs) 'a) (car (cdr (cdr stepargs))) '() (replicate (car (cdr stepargs)) 'a) claim)
                  (scp-steps defs fullscp (replicate (car stepargs) 'a) '() (car (cdr (cdr (cdr stepargs)))) (replicate (car (cdr stepargs)) 'a) claim))))))

(defun expand-insert-Q-step (stepargs claim)
  (if (list2? stepargs)
      (if (focus-is-at-path?2 (elem1 stepargs) claim)
          (list1 (list2 (elem1 stepargs)
                        (app-c 'if-same (list2 (elem2 stepargs) (find-focus-at-path2 (elem1 stepargs) claim)))))
          '())
      '()))

(defun expand-expand-step (defs stepargs claim)
  (if (list2? stepargs)
      (list1 (list2 (expand-path defs (elem1 stepargs) claim) (elem2 stepargs)))
      '()))

(defun expand-proof-steps-helper (defs prevsteps claim steps)
  (cons (append prevsteps steps) (rewrite/steps defs claim steps)))

(defun expand-proof-steps (defs prevsteps_claim steps)
  (if (atom steps)
      (car prevsteps_claim)
      (if (atom (car steps))
          (car prevsteps_claim)
          (if (atom (car (car steps)))
              (if (equal (car (car steps)) '())
                  (append (car prevsteps_claim)
                          (cons (car steps)
                                (expand-proof-steps defs
                                                    (cons '() (rewrite/step defs (cdr prevsteps_claim) (car steps)))
                                                    (cdr steps))))
                  (if (equal (car (car steps)) 'scp)
                      (expand-proof-steps defs
                                          (expand-proof-steps-helper defs 
                                                                     (car prevsteps_claim)
                                                                     (cdr prevsteps_claim)
                                                                     (expand-scp-step defs 't (cdr (car steps)) (cdr prevsteps_claim)))
                                          (cdr steps))
                      (if (equal (car (car steps)) 'simpl)
                          (expand-proof-steps defs
                                              (expand-proof-steps-helper defs 
                                                                         (car prevsteps_claim)
                                                                         (cdr prevsteps_claim)
                                                                         (expand-scp-step defs 'nil (cdr (car steps)) (cdr prevsteps_claim)))
                                              (cdr steps))
                          (if (equal (car (car steps)) 'insert-Q)
                              (expand-proof-steps defs
                                                  (expand-proof-steps-helper defs
                                                                             (car prevsteps_claim)
                                                                             (cdr prevsteps_claim)
                                                                             (expand-insert-Q-step (cdr (car steps)) (cdr prevsteps_claim)))
                                                  (cdr steps))
                              (if (equal (car (car steps)) 'expand)
                                  (expand-proof-steps defs
                                                      (expand-proof-steps-helper defs
                                                                                 (car prevsteps_claim)
                                                                                 (cdr prevsteps_claim)
                                                                                 (expand-expand-step defs (cdr (car steps)) (cdr prevsteps_claim)))
                                                      (cdr steps))
                                  (if (equal (car (car steps)) 'fake-proof)
                                      (append
                                       (car prevsteps_claim)
                                       (list1 (list2 '() (app-c 'fake-equality (list2 (cdr prevsteps_claim) ''t)))))
                                      (car prevsteps_claim)))))))
              (append (car prevsteps_claim)
                      (cons (car steps)
                            (expand-proof-steps defs
                                                (cons '() (rewrite/step defs (cdr prevsteps_claim) (car steps)))
                                                (cdr steps))))))))

;(defun J-Bob/prove_ (defs prfs) (J-Bob/prove defs prfs))

(defun expand-proof (defs prf)
  (begin
    (display "expanding proof of: ")
    (display (elem2 (elem1 prf)))
    (newline)
;    (time
     ;;
     (cons (append defs (list1 (elem1 prf)))
           (list1 (cons (elem1 prf)
                        (cons (elem2 prf)
                              (expand-proof-steps defs
                                                  (cons '() (J-Bob/prove defs (list1 (list2 (elem1 prf) (elem2 prf)))))
                                                  (cdr (cdr prf)))))))
;     )
    )
  )

(defun expand-proofs-rec (defs_optexpandedprf prfs)
  (if (atom prfs)
      (cdr defs_optexpandedprf)
      (append (cdr defs_optexpandedprf)
              (expand-proofs-rec (expand-proof (car defs_optexpandedprf) (car prfs))
                                 (cdr prfs)))))

(defun expand-proofs (defs prfs)
  (expand-proofs-rec (cons defs '()) prfs))

;;;

;(expand-path
; '((defun g (x y) x) (defun ff (x) x))
; '(A (ff 2) 1)
; '(if x (g 'a (g (ff '3) (ff '4))) 'nil))
;
;(J-Bob/prove (prelude)
;             (expand-proofs (prelude)
;                            '(((defun app (xs ys)
;                                 (if (atom xs)
;                                     ys
;                                     (cons (car xs) (app (cdr xs) ys))))
;                               (size xs)
;                               (scp 0 10)
;                               ((E) (size/cdr xs))
;                               (scp 0 10)
;                               ;                              ((Q) (natp/size xs))
;                               ;                              (() (if-true (if (atom xs) 't (< (size (cdr xs)) (size xs))) 'nil))
;                               ;                              ((E) (size/cdr xs))
;                               ;                              (() (if-same (atom xs) 't))
;                               )
;                              ((dethm app-assoc (xs ys zs)
;                                      (equal (app (app xs ys) zs) (app xs (app ys zs))))
;                               (list-induction xs)
;                               (scp 5 10)
;                               ((E A 1 2) (equal-if (app (app (cdr xs) ys) zs) (app (cdr xs) (app ys zs))))
;                               (scp 0 10)
;                               )
;                              ((defun length (xs)
;                                 (if (atom xs) '0 (+ '1 (length (cdr xs)))))
;                               (size xs)
;                               (scp 0 10)
;                               ((E) (size/cdr xs))
;                               (scp 0 10)
;                               )
;                              ((dethm length-app-atom-r (xs ys)
;                                      (if (atom ys) (equal (length (app xs ys)) (length xs)) 't))
;                               (list-induction xs)
;                               (scp 5 10)
;                               ((E A A 1 2) (equal-if (length (app (cdr xs) ys)) (length (cdr xs))))
;                               (scp 1 10)
;                               )
;                              ((dethm length-app-cons-r (xs ys)
;                                      (if (atom ys) 't (equal (length (app xs ys)) (+ '1 (length (app xs (cdr ys)))))))
;                               (list-induction xs)
;                               (scp 5 10)
;                               ((E E A 1 2) (equal-if (length (app (cdr xs) ys)) (+ '1 (length (app (cdr xs) (cdr ys))))))
;                               (scp 2 10)
;                               )
;                              ((dethm app-len-comm (xs ys)
;                                      (equal (length (app xs ys)) (length (app ys xs))))
;                               (list-induction xs)
;                               (scp 3 10)
;                               ((A 2) (length-app-atom-r ys xs))
;                               (scp 0 10)
;                               ((E A 1 2) (equal-if (length (app (cdr xs) ys)) (length (app ys (cdr xs)))))
;                               ((E A 2) (length-app-cons-r ys xs))
;                               (scp 0 10)
;                               )
;                              )
;                            )
;             )
;
;(J-Bob/prove
; (append (prelude) '((dethm fake-equality (x y) (equal x y))))
; (expand-proofs
;  (append (prelude) '((dethm fake-equality (x y) (equal x y))))
;  '(((defun app (xs ys)
;       (if (atom xs)
;           ys
;           (cons (car xs) (app (cdr xs) ys))))
;     (size xs)
;     (scp 0 10)
;     ((E) (size/cdr xs))
;     (scp 0 10)
;     )
;    ((dethm app-assoc (xs ys zs)
;            (equal (app (app xs ys) zs) (app xs (app ys zs))))
;     (list-induction xs)
;     (fake-proof)
;     ;(scp 2 10 () (app))
;     ;(scp 2 10 (app))
;     (scp 5 10)
;     (expand ((app 5)) (equal-if (app (app (cdr xs) ys) zs) (app (cdr xs) (app ys zs))))
;     ;(scp 0 10)
;     )
;    )
;  )
; )

