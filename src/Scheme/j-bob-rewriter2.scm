;; a partial copy of https://github.com/the-little-prover/j-bob/blob/1f68e43ea36c2108f87c83c22e88bb14d9eeb60b/scheme/j-bob.scm,
;; containing only `rewrite/steps` and the definitions it uses


;(load "../j-bob/scheme/j-bob-lang.scm")


;;;

(defun find-focus-at-direction2 (dir b_e)
  (if (car b_e)
    (if (equal dir 'Q)
      (cons 't (if.Q (cdr b_e)))
      (if (equal dir 'A)
        (cons 't (if.A (cdr b_e)))
        (if (equal dir 'E)
          (cons 't (if.E (cdr b_e)))
          (cons 't (get-arg dir (app.args (cdr b_e)))))))
    b_e))

(defun rewrite-focus-at-direction2 (dir b_e1 b_e2)
  (if (equal (car b_e1) 't)
    (if (equal (car b_e2) 't)
      (if (equal dir 'Q)
        (cons 't (if-c (cdr b_e2) (if.A (cdr b_e1)) (if.E (cdr b_e1))))
        (if (equal dir 'A)
          (cons 't (if-c (if.Q (cdr b_e1)) (cdr b_e2) (if.E (cdr b_e1))))
          (if (equal dir 'E)
            (cons 't (if-c (if.Q (cdr b_e1)) (if.A (cdr b_e1)) (cdr b_e2)))
            (cons 't (app-c (app.name (cdr b_e1))
              (set-arg dir (app.args (cdr b_e1)) (cdr b_e2)))))))
      (cons 'nil (cdr b_e1)))
    (cons 'nil (cdr b_e1))))

(defun focus-is-at-path?2 (path e)
  (if (atom path)
    't
    (if (focus-is-at-direction? (car path) e)
      (focus-is-at-path?2 (cdr path)
        (cdr (find-focus-at-direction2 (car path) (cons 't e))))
      'nil)))

(defun find-focus-at-path2 (path e)
  (if (atom path)
    e
    (find-focus-at-path2 (cdr path)
      (cdr (find-focus-at-direction2 (car path) (cons 't e))))))

(defun rewrite-focus-at-path2 (path b_e1 b_e2)
  (if (equal (car b_e1) 't)
    (if (equal (car b_e2) 't)
      (if (atom path)
        b_e2
        (rewrite-focus-at-direction2 (car path) b_e1
          (rewrite-focus-at-path2 (cdr path)
            (find-focus-at-direction2 (car path) b_e1)
            b_e2)))
      (cons 'nil (cdr b_e1)))
    (cons 'nil (cdr b_e1))))

(defun prem-A?2 (prem path e)
  (if (atom path)
    'nil
    (if (equal (car path) 'A)
      (if (equal (if.Q e) prem)
        't
        (prem-A?2 prem (cdr path)
          (cdr (find-focus-at-direction2 (car path)
            (cons 't e)))))
      (prem-A?2 prem (cdr path)
        (cdr (find-focus-at-direction2 (car path)
          (cons 't e)))))))

(defun prem-E?2 (prem path e)
  (if (atom path)
    'nil
    (if (equal (car path) 'E)
      (if (equal (if.Q e) prem)
        't
        (prem-E?2 prem (cdr path)
          (cdr (find-focus-at-direction2 (car path)
            (cons 't e)))))
      (prem-E?2 prem (cdr path)
        (cdr (find-focus-at-direction2 (car path)
          (cons 't e)))))))

(defun follow-prems2 (path e thm)
  (if (if? thm)
    (if (prem-A?2 (if.Q thm) path e)
      (follow-prems2 path e (if.A thm))
      (if (prem-E?2 (if.Q thm) path e)
        (follow-prems2 path e (if.E thm))
        thm))
    thm))

;;;

(defun equality2 (focus a b)
  (if (equal focus a)
    (cons 't b)
    (if (equal focus b)
      (cons 't a)
      (cons 'nil focus))))

(defun equality/equation2 (focus concl-inst)
  (if (app-of-equal? concl-inst)
    (equality2 focus
      (elem1 (app.args concl-inst))
      (elem2 (app.args concl-inst)))
    (cons 'nil focus)))

(defun equality/path2 (e path thm)
  (if (focus-is-at-path?2 path e)
    (rewrite-focus-at-path2 path (cons 't e)
      (equality/equation2
        (find-focus-at-path2 path e)
        (follow-prems2 path e thm)))
    (cons 'nil e)))

(defun equality/def2 (claim path app def)
  (if (rator? def)
    (equality/path2 claim path
      (app-c 'equal (list2 app (eval-op app))))
    (if (defun? def)
      (equality/path2 claim path
        (sub-e (defun.formals def)
          (app.args app)
          (app-c 'equal
            (list2
              (app-c (defun.name def)
                (defun.formals def))
              (defun.body def)))))
      (if (dethm? def)
        (equality/path2 claim path
          (sub-e (dethm.formals def)
            (app.args app)
            (dethm.body def)))
        (cons 'nil claim)))))

(defun rewrite/step2 (defs claim step)
  (equality/def2 claim (elem1 step) (elem2 step)
    (lookup (app.name (elem2 step)) defs)))

(defun rewrite/continue2 (defs steps old new)
  (if (car new)
    (if (atom steps)
      new
      (rewrite/continue2 defs (cdr steps) (cdr new)
        (rewrite/step2 defs (cdr new) (car steps))))
    new))

(defun rewrite/steps2 (defs claim steps)
  (if (atom steps)
    (cons 't claim)
    (rewrite/continue2 defs (cdr steps) claim
      (rewrite/step2 defs claim (car steps)))))

;;;

;(rewrite/steps2 (axioms) '(car (cons a b)) '((() (car/cons a b))))
;(rewrite/steps2 (axioms) 'a '((() (car/cons a b))))
;(rewrite/steps2 '((defun f1 (x) (cons x x))) '(f1 '1) '((() (f1 '1))))
;(rewrite/steps2 '((defun f1 (x) (cons x x))) '(cons '1 '1) '((() (f1 '1))))
;(rewrite/steps2 '() '(+ '1 '2) '((() (+ '1 '2))))
;(rewrite/steps2 '() ''3 '((() (+ '1 '2))))
;(rewrite/steps2 (axioms) '(if x '1 (if x '2 '3)) '(((E) (if-nest-E x '2 '3))))
;(rewrite/steps2 (axioms) '(if x '1 '3) '(((E) (if-nest-E x '2 '3))))
;(rewrite/steps2 (axioms) '(f '1 (car (cons a b))) '(((2) (car/cons a b))))
;(rewrite/steps2 (axioms) '(f '1 a) '(((2) (car/cons a b))))

