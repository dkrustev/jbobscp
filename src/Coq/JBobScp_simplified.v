(* author: Dimitur Krustev *)
(* started: 20160313 *)

Require Import Arith List String.

Set Implicit Arguments.

Lemma length_firstn: forall A n (xs: list A),
  List.length (firstn n xs) = min n (List.length xs).
Proof.
  induction n; auto.
  destruct xs; auto.
  simpl. f_equal; auto.
Qed.

Lemma skipn_S: forall A n (xs: list A),
  skipn (S n) xs = tl (skipn n xs).
Proof.
  induction n; auto.
  remember (S n) as n1.
  simpl. destruct xs.
  - subst. reflexivity.
  - subst. unfold skipn at 2. fold (skipn n xs). auto.
Qed.

Lemma ifSame: forall X (b: bool) (x: X), (if b then x else x) = x.
Proof.
  destruct b; auto.
Qed.

Lemma map_In_ext: forall X Y (f g: X -> Y) xs, (forall x, In x xs -> f x = g x) ->
  map f xs = map g xs.
Proof.
  induction xs; auto.
  (* a::xs *) rename a into x. simpl. intros. f_equal.
    (* left *) apply H. left. reflexivity.
    (* right *) apply IHxs. intros. apply H. right. assumption.
Qed.

(* ***** *)

Fixpoint listMember {A} (A_dec: forall x y: A, {x = y} + {x <> y})
  (x: A) (xs: list A) : bool :=
  match xs with
  | nil => false
  | y::xs => if A_dec x y then true else listMember A_dec x xs
  end.

Fixpoint listSubset {A} (A_dec: forall x y: A, {x = y} + {x <> y})
  (xs ys: list A) : bool :=
  match xs with
  | nil => true
  | x::xs => andb (listMember A_dec x ys) (listSubset A_dec xs ys)
  end.

(* ***** *)
(* A partial model of the J-Bob prover (https://github.com/the-little-prover/j-bob) - datatypes *)

Inductive Val: Set := VNat (n: nat) | VSym (sym: string) | VCons (h t: Val).

Fixpoint valSize (v: Val) : nat := 
  match v with
  | VCons h t => S (valSize h + valSize t)
  | _ => 0
  end.

Inductive Builtin1: Set := Car | Cdr | Atom | Natp | Size.

Inductive Builtin2: Set := Cons | Equal | Plus | LT.

Inductive Application (Exp: Set): Set := 
  | AOp1 (op: Builtin1) (arg: Exp)
  | AOp2 (op: Builtin2) (arg1 arg2: Exp)
  | AFun (funRef: string) (args: list Exp).

Inductive Exp: Set := EVar (x: string) | EQuote (v: Val)
  | EIf (q a e: Exp) | EApp (a: Application Exp).

Definition Val_eq_dec: forall x y: Val, {x = y} + {x <> y}.
  decide equality.
  - apply eq_nat_dec.
  - apply string_dec.
Defined.

Definition Exp_fullInd (P: Exp -> Prop) (PVar: forall x, P (EVar x))
  (PQuote: forall v, P (EQuote v))
  (PIf: forall q a e, P q -> P a -> P e -> P (EIf q a e))
  (PAppOp1: forall op arg, P arg -> P (EApp (AOp1 op arg)))
  (PAppOp2: forall op arg1 arg2, P arg1 -> P arg2 -> P (EApp (AOp2 op arg1 arg2)))
  (PAppFun: forall fname args, Forall P args -> P (EApp (AFun fname args)))
  : forall e: Exp, P e :=
  fix F (e: Exp) : P e :=
    match e return P e with
    | EVar x => PVar x
    | EQuote v => PQuote v
    | EIf q a e => PIf _ _ _ (F q) (F a) (F e)
    | EApp (AOp1 op arg) => PAppOp1 _ _ (F arg)
    | EApp (AOp2 op arg1 arg2) => PAppOp2 _ _ _ (F arg1) (F arg2)
    | EApp (AFun fname args) => 
        PAppFun _ _ 
          ((fix F1 (es: list Exp) : Forall P es :=
            match es with
            | nil => Forall_nil _
            | e::es => Forall_cons _ (F e) (F1 es)
            end)
           args)
    end.

Definition Builtin1_eq_dec: forall x y: Builtin1, {x = y} + {x <> y}.
  decide equality.
Defined.

Definition Builtin2_eq_dec: forall x y: Builtin2, {x = y} + {x <> y}.
  decide equality.
Defined.

Fixpoint expEqb (e1 e2: Exp) {struct e1} : bool :=
  let expsEqb := fix expsEqb (es1 es2: list Exp) {struct es1} : bool :=
    match es1, es2 with
    | nil, nil => true
    | e1::es1, e2::es2 => andb (expEqb e1 e2) (expsEqb es1 es2)
    | _, _ => false
    end in
  match e1, e2 with
  | EVar x, EVar y => if string_dec x y then true else false
  | EQuote v1, EQuote v2 => if Val_eq_dec v1 v2 then true else false
  | EIf q1 a1 e1, EIf q2 a2 e2 => 
      andb (expEqb q1 q2) (andb (expEqb a1 a2) (expEqb e1 e2))
  | EApp (AOp1 op1 arg1), EApp (AOp1 op2 arg2) =>
      if Builtin1_eq_dec op1 op2 then expEqb arg1 arg2 else false
  | EApp (AOp2 op1 arg11 arg12), EApp (AOp2 op2 arg21 arg22) =>
      if Builtin2_eq_dec op1 op2 
      then andb (expEqb arg11 arg21) (expEqb arg12 arg22)
      else false
  | EApp (AFun name1 args1), EApp (AFun name2 args2) =>
      if string_dec name1 name2 then expsEqb args1 args2 else false
  | _, _ => false
  end.  

Lemma expEqb_refl: forall e, expEqb e e = true.
Proof.
  induction e using Exp_fullInd; simpl.
  - destruct (string_dec x x); congruence.
  - destruct (Val_eq_dec v v); congruence.
  - rewrite Bool.andb_true_iff. split; auto.
    rewrite Bool.andb_true_iff. split; auto.
  - destruct (Builtin1_eq_dec op op); congruence.
  - destruct (Builtin2_eq_dec op op); try congruence.
    rewrite Bool.andb_true_iff. split; auto.
  - destruct (string_dec fname fname); try congruence.
    clear fname e.
    rewrite Forall_forall in *.
    induction args; auto.
    rewrite Bool.andb_true_iff. split.
    + apply H. simpl. left. reflexivity.
    + apply IHargs. intros e Hin. apply H. simpl. right. assumption.
Qed.

Lemma expEqb_true_eq: forall e1 e2, expEqb e1 e2 = true -> e1 = e2.
Proof.
  induction e1 using Exp_fullInd; destruct e2; simpl; try congruence.
  - destruct (string_dec x x0); congruence.
  - destruct (Val_eq_dec v v0); congruence.
  - intro Heq. repeat (rewrite Bool.andb_true_iff in * ). 
    destruct Heq as [Heq1 [Heq2 Heq3]].
    f_equal; auto.
  - destruct a; try congruence.
    destruct (Builtin1_eq_dec op op0); try congruence. subst.
    intro Heq. f_equal; auto. f_equal; auto.
  - destruct a; try congruence.
    destruct (Builtin2_eq_dec op op0); try congruence. subst.
    rewrite Bool.andb_true_iff. intros [Heq1 Heq2].
    f_equal. f_equal; auto.
  - destruct a; try congruence.
    destruct (string_dec fname funRef); try congruence. subst.
    revert H args0.
    induction args as [|arg args IHargs].
    + destruct args0; try congruence.
    + destruct args0; try congruence.
      rewrite Bool.andb_true_iff. intros [Heq1 Heq2].
      rewrite Forall_forall in *.
      rewrite H with (x:=arg)(e2:=e); auto.
      2: simpl; left; reflexivity.
      f_equal. f_equal. f_equal.
      assert (H': forall x : Exp, In x args -> forall e2 : Exp, expEqb x e2 = true -> x = e2).
      { intros. apply H; auto. simpl. right. assumption. }
      specialize (IHargs H' args0 Heq2).
      inversion IHargs. reflexivity.
Qed.

Definition builtin1Name (op: Builtin1) : string :=
  (match op with
  | Car => "car"
  | Cdr => "cdr"
  | Atom => "atom"
  | Natp => "natp"
  | Size => "size"
  end)%string.

Definition builtin2Name (op: Builtin2) : string :=
  (match op with
  | Cons => "cons"
  | Equal => "equal"
  | Plus => "+"
  | LT => "<"
  end)%string.

Fixpoint vals2val (vs: list Val) : Val :=
  match vs with
  | nil => VSym "nil"
  | v::vs => VCons v (vals2val vs)
  end.

Fixpoint exp2val (e: Exp) : Val := 
  match e with
  | EVar x => VSym x
  | EQuote v => VCons (VSym "quote") v
  | EIf q a e => 
      VCons (VSym "if") (VCons (exp2val q) (VCons (exp2val a) (VCons (exp2val e) (VSym "nil"))))
  | EApp (AOp1 op arg) =>
      VCons (VSym (builtin1Name op)) (VCons (exp2val arg) (VSym "nil"))
  | EApp (AOp2 op arg1 arg2) =>
      VCons (VSym (builtin2Name op)) (VCons (exp2val arg1) (VCons (exp2val arg2) (VSym "nil")))
  | EApp (AFun name args) =>
      VCons (VSym name) (vals2val (map exp2val args))
  end.

(* Definition expSize e := valSize (exp2val e). *)

Fixpoint expSize (e: Exp) : nat :=
  match e with
  | EVar _ => 0
  | EQuote v => S (valSize v)
  | EIf q a e => S (expSize q + expSize a + expSize e)
  | EApp (AOp1 _ arg) => S (expSize arg)
  | EApp (AOp2 _ arg1 arg2) => S (expSize arg1 + expSize arg2)
  | EApp (AFun _ args) => S (fold_right (fun e size => size + expSize e) 0 args)
  end.

Inductive DefKind: Set := Fun | Thm.

Inductive Def: Set := De (kind: DefKind) (name: string) (formals: list string) (body: Exp).

Definition DefKind_eq_dec: forall x y: DefKind, {x = y} + {x <> y}.
  decide equality.
Defined.

Definition Def_eq_dec: forall x y: Def, {x = y} + {x <> y}.
  decide equality.
  - destruct (expEqb body body0) eqn: Heq.
    + apply expEqb_true_eq in Heq. subst. auto.
    + right. intro Hcontra. subst. rewrite expEqb_refl in Heq. congruence.
  - apply (list_eq_dec string_dec).
  - apply string_dec.
  - apply DefKind_eq_dec.
Defined.

Definition appName e :=
  match e with
  | EApp (AFun name _) => name
  | _ => ""%string
  end.

Fixpoint lookup (name: string) (defs: list Def) : option Def :=
  match defs with
  | nil => None
  | (De _ name' _ _) as def :: defs =>
      if string_dec name name' then Some def else lookup name defs
  end.

Inductive Direction: Set := DirQ | DirA | DirE | DirNum (n: nat).

Definition Direction_eq_dec: forall x y: Direction, {x = y} + {x <> y}.
  decide equality. apply eq_nat_dec.
Defined.

Definition Path: Set := list Direction.

Definition Step: Set := (Path * Exp)%type.

(* ***** *)
(* A partial model of the J-Bob prover (https://github.com/the-little-prover/j-bob) - rewriting steps *)

Fixpoint getFunArg (n: nat) (args: list Exp) {struct args} : option Exp :=
  match args with
  | nil => None
  | arg::args => match n with
    | 0 => None
    | 1 => Some arg
    | S n => getFunArg n args
    end
  end.

Definition getArg (n: nat) (a: Application Exp) : option Exp :=
  match a with
  | AOp1 op arg => match n with
    | 1 => Some arg
    | _ => None
    end
  | AOp2 op arg1 arg2 => match n with
    | 1 => Some arg1
    | 2 => Some arg2
    | _ => None
    end
  | AFun name args => getFunArg n args 
  end.

Fixpoint setFunArg (n: nat) (args: list Exp) (e: Exp) {struct args} : option (list Exp) :=
  match args with
  | nil => None
  | arg::args => match n with
    | 0 => None
    | 1 => Some (e::args)
    | S n => option_map (fun args => arg::args) (setFunArg n args e)
    end
  end.

Definition setArg (n: nat) (a: Application Exp) (e: Exp) : option Exp :=
  match a with
  | AOp1 op arg => match n with
    | 1 => Some (EApp (AOp1 op e))
    | _ => None
    end
  | AOp2 op arg1 arg2 => match n with
    | 1 => Some (EApp (AOp2 op e arg2))
    | 2 => Some (EApp (AOp2 op arg1 e))
    | _ => None
    end
  | AFun name args => 
      option_map (fun args => EApp (AFun name args)) (setFunArg n args e)
  end.

(* 397 *)
Fixpoint subVar (vars: list string) (args: list Exp) (x: string) : Exp :=
  match vars, args with
  | y::vars, e::args => if string_dec x y then e else subVar vars args x
  | _, _ => EVar x
  end.

Fixpoint subE (vars: list string) (args: list Exp) (e: Exp) : Exp :=
  match e with
  | EVar x => subVar vars args x
  | EQuote _ => e
  | EIf q a els => EIf (subE vars args q) (subE vars args a) (subE vars args els)
  | EApp (AOp1 op arg) => EApp (AOp1 op (subE vars args arg))
  | EApp (AOp2 op arg1 arg2) => EApp (AOp2 op (subE vars args arg1) (subE vars args arg2))
  | EApp (AFun name args') => EApp (AFun name (map (subE vars args) args'))
  end.
(* 427 *)

(* 526 *)
Definition findFocusAtDirection dir (e: bool * Exp) : bool * Exp :=
  match dir, snd e with
  | DirQ, EIf q _ _ => (true, q)
  | DirA, EIf _ a _ => (true, a)
  | DirE, EIf _ _ e => (true, e)
  | DirNum n, EApp a => match getArg n a with
    | Some e' => (true, e')
    | None => (false, snd e)
    end
  | _, _ => (false, snd e)
  end.

Definition rewriteFocusAtDirection dir (e1 e2: bool * Exp) :=
  if andb (fst e1) (fst e2) then
    match dir, snd e1 with
    | DirQ, EIf _ a e => (true, EIf (snd e2) a e)
    | DirA, EIf q _ e => (true, EIf q (snd e2) e)
    | DirE, EIf q a _ => (true, EIf q a (snd e2))
    | DirNum n, EApp a => match setArg n a (snd e2) with
      | Some e => (true, e)
      | None => (false, snd e1)
      end
    | _, _ => (false, snd e1)
    end
  else (false, snd e1).

Definition focusIsAtDirection dir e :=
  match dir with
  | DirQ | DirA | DirE => match e with EIf _ _ _ => true | _ => false end
  | DirNum n => match e with
    | EApp (AOp1 _ _) => match n with 1 => true | _ => false end
    | EApp (AOp2 _ _ _) => match n with 1 | 2 => true | _ => false end
    | EApp (AFun _ args) => match n with
      | 0 => false
      | _ => if le_lt_dec n (List.length args) then true else false
      end
    | _ => false
    end
  end.

Fixpoint focusIsAtPath (path: Path) (e: Exp) : bool :=
  match path with
  | nil => true
  | dir::path => 
    if focusIsAtDirection dir e 
    then focusIsAtPath path (snd (findFocusAtDirection dir (true, e)))
    else false
  end.

Fixpoint findFocusAtPath (path: Path) (e: Exp) : Exp :=
  match path with
  | nil => e
  | dir::path => findFocusAtPath path (snd (findFocusAtDirection dir (true, e)))
  end.

Fixpoint rewriteFocusAtPath (path: Path) (e1 e2: bool * Exp) : bool * Exp :=
  if andb (fst e1) (fst e2) then
    match path with
    | nil => e2
    | dir::path => 
      rewriteFocusAtDirection dir e1 (rewriteFocusAtPath path 
        (findFocusAtDirection dir e1) e2)
    end
  else (false, snd e1).

Fixpoint premA (prem: Exp) (path: Path) (e: Exp) : bool :=
  match path, e with
  | nil, _ => false
  | DirA::path, EIf q _ _ => 
      if expEqb q prem then true
      else premA prem path (snd (findFocusAtDirection DirA (true, e)))
  | dir::path, _ => premA prem path (snd (findFocusAtDirection dir (true, e)))
  end.

Fixpoint premE (prem: Exp) (path: Path) (e: Exp) : bool :=
  match path, e with
  | nil, _ => false
  | DirE::path, EIf q _ _ => 
      if expEqb q prem then true
      else premE prem path (snd (findFocusAtDirection DirE (true, e)))
  | dir::path, _ => premE prem path (snd (findFocusAtDirection dir (true, e)))
  end.

Fixpoint followPrems (path: Path) (e: Exp) (thm: Exp) : Exp :=
  match thm with
  | EIf q a els => 
    if premA q path e then followPrems path e a
    else if premE q path e then followPrems path e els
    else thm
  | _ => thm
  end.

Definition applyOp1 op v :=
  match op with
  | Car => match v with VCons h _ => h | _ => VSym "nil" end
  | Cdr => match v with VCons _ t => t | _ => VSym "nil" end
  | Atom => match v with VCons _ _ => VSym "nil" | _ => VSym "t" end
  | Natp => match v with VNat _ => VSym "t" | _ => VSym "nil" end
  | Size => VNat (valSize v)
  end.  

Definition applyOp2 op v1 v2 :=
  let val2nat v := match v with VNat n => n | _ => 0 end in
  match op with
  | Cons => VCons v1 v2
  | Equal => if Val_eq_dec v1 v2 then VSym "t" else VSym "nil"
  | Plus => VNat (val2nat v1 + val2nat v2)
  | LT => if lt_dec (val2nat v1) (val2nat v2) then VSym "t" else VSym "nil"
  end.

Definition applyOp app :=
  match app with
  | EApp (AOp1 op (EQuote v)) => applyOp1 op v
  | EApp (AOp2 op (EQuote v1) (EQuote v2)) => applyOp2 op v1 v2
  | _ => VSym "nil"
  end.

Definition evalOp app := EQuote (applyOp app).

Definition equality focus a b :=
  if expEqb focus a then (true, b)
  else if expEqb focus b then (true, a)
  else (false, focus).

Definition equalityEquation (focus: Exp) (conclInst: Exp) :=
  match conclInst with
  | EApp (AOp2 Equal e1 e2) => equality focus e1 e2
  | _ => (false, focus)
  end.

Definition equalityPath (e: Exp) (path: Path) (thm: Exp) :=
  if focusIsAtPath path e
  then rewriteFocusAtPath path (true, e)
    (equalityEquation (findFocusAtPath path e) (followPrems path e thm))
  else (false, e).

Definition equalityDef (claim: Exp) (path: Path) (app: Exp) (def: option Def) :=
  match app, def with
  | EApp (AOp1 _ _), _ | EApp (AOp2 _ _ _), _ => 
      equalityPath claim path (EApp (AOp2 Equal app (evalOp app)))
  | EApp (AFun _ args), Some (De Fun name formals body) =>
      equalityPath claim path 
        (subE formals args (EApp (AOp2 Equal (EApp (AFun name (map EVar formals))) body)))
  | EApp (AFun _ args), Some (De Thm name formals body) =>
      equalityPath claim path (subE formals args body)
  | _, _ => (false, claim)
  end.

Definition rewriteStep (defs: list Def) (claim: Exp) (step: Step) :=
  equalityDef claim (fst step) (snd step) (lookup (appName (snd step)) defs).

Fixpoint rewriteContinue (defs: list Def) (steps: list Step) (old: Exp)
  (new: bool * Exp) : bool * Exp :=
  if fst new then
    match steps with
    | nil => new
    | step::steps => rewriteContinue defs steps (snd new) (rewriteStep defs (snd new) step)
    end
  else new.

Definition rewriteSteps defs claim steps :=
  match steps with
  | nil => (true, claim)
  | step::steps => rewriteContinue defs steps claim (rewriteStep defs claim step)
  end.
(* 721 *)

Definition axioms: list Def :=
  De Thm "atom/cons" ("x"::"y"::nil)%string
    (EApp (AOp2 Equal (EApp (AOp1 Atom (EApp (AOp2 Cons (EVar "x") (EVar "y"))))) 
                      (EQuote (VSym "nil"))))
  ::
  De Thm "car/cons" ("x"::"y"::nil)%string
    (EApp (AOp2 Equal (EApp (AOp1 Car (EApp (AOp2 Cons (EVar "x") (EVar "y"))))) (EVar "x")))
  ::
  De Thm "cdr/cons" ("x"::"y"::nil)%string
    (EApp (AOp2 Equal (EApp (AOp1 Cdr (EApp (AOp2 Cons (EVar "x") (EVar "y"))))) (EVar "y")))
  ::
  De Thm "equal-same" ("x"::nil)%string
    (EApp (AOp2 Equal (EApp (AOp2 Equal (EVar "x") (EVar "x"))) (EQuote (VSym "t"))))
  ::
  De Thm "if-same" ("x"::"y"::nil)%string
    (EApp (AOp2 Equal (EIf (EVar "x") (EVar "y") (EVar "y")) (EVar "y")))
  ::
  De Thm "if-true" ("x"::"y"::nil)%string
    (EApp (AOp2 Equal (EIf (EQuote (VSym "t")) (EVar "x") (EVar "y")) (EVar "x")))
  ::
  De Thm "if-false" ("x"::"y"::nil)%string
    (EApp (AOp2 Equal (EIf (EQuote (VSym "nil")) (EVar "x") (EVar "y")) (EVar "y")))
  ::
  De Thm "if-nest-E" ("x"::"y"::"z"::nil)%string
    (EIf (EVar "x") 
         (EQuote (VSym "t")) 
         (EApp (AOp2 Equal (EIf (EVar "x") (EVar "y") (EVar "z")) (EVar "z"))))
  ::
  De Thm "if-nest-A" ("x"::"y"::"z"::nil)%string
    (EIf (EVar "x")  
         (EApp (AOp2 Equal (EIf (EVar "x") (EVar "y") (EVar "z")) (EVar "y")))
         (EQuote (VSym "t")))
  ::
  De Thm "natp/size" ("x"::nil)%string
    (EApp (AOp2 Equal (EApp (AOp1 Natp (EApp (AOp1 Size (EVar "x"))))) (EQuote (VSym "t"))))
  ::
  nil.

(* ***** *)
(* ***** *)
(* ***** *)
(* Prototype of a simple supercompiler generating J-Bob proofs *)

Definition listOptPlus {A} (o1 o2: list A) : list A :=
  match o1 with
  | nil => o2
  | _::_ => o1
  end.

Definition ifLiftingSteps (path pathsuffix: Path) (exp q e a: Exp) : list Step :=
  (path, EApp (AFun "if-same" (q::exp::nil)))
  :: (path ++ DirA :: pathsuffix, EApp (AFun "if-nest-A" (q::e::a::nil)))
  :: (path ++ DirE :: pathsuffix, EApp (AFun "if-nest-E" (q::e::a::nil)))
  :: nil.

Definition contextGoodForIfLifting (dir: Direction) : bool :=
  match dir with
  | DirNum _ => true
  | DirQ => true
  | _ => false
  end.

Definition tryIfLifting (eroot: Exp) (path: Path) (dir: Direction) (q a e: Exp) : list Step :=
  if contextGoodForIfLifting dir
  then ifLiftingSteps path (dir::nil) (findFocusAtPath path eroot) q a e
  else nil.

Definition simplifyIf (fullscp: bool) (eroot: Exp) (path: Path) (q a e: Exp) : list Step :=
  if expEqb q (EQuote (VSym "t")) 
  then (path, EApp (AFun "if-true" (a::e::nil)))::nil
  else if expEqb q (EQuote (VSym "nil")) 
  then (path, EApp (AFun "if-false" (a::e::nil)))::nil
  else 
  if fullscp then
    if premA q path eroot
    then (path, EApp (AFun "if-nest-A" (q::a::e::nil)))::nil
    else if premE q path eroot
    then (path, EApp (AFun "if-nest-E" (q::a::e::nil)))::nil
    else if expEqb a e 
    then (path, EApp (AFun "if-same" (q::a::nil)))::nil
    else 
      match path with
      | nil => nil
      | _::_ => tryIfLifting eroot (removelast path) (last path DirA) q a e
      end
  else nil.
  
Definition simplifyAtom (path: Path) (arg: Exp) : list Step :=
  match arg with
  | EApp (AOp2 Cons arg1 arg2) =>
      (path, EApp (AFun "atom/cons" (arg1::arg2::nil)))::nil
  | _ => nil
  end.

Definition simplifyCar (path: Path) (arg: Exp) : list Step :=
  match arg with
  | EApp (AOp2 Cons arg1 arg2) =>
      (path, EApp (AFun "car/cons" (arg1::arg2::nil)))::nil
  | _ => nil
  end.
  
Definition simplifyCdr (path: Path) (arg: Exp) : list Step :=
  match arg with
  | EApp (AOp2 Cons arg1 arg2) =>
      (path, EApp (AFun "cdr/cons" (arg1::arg2::nil)))::nil
  | _ => nil
  end.
  
Definition simplifyEqual (path: Path) (arg1 arg2: Exp) : list Step :=
  if expEqb arg1 arg2
  then (path, EApp (AFun "equal-same" (arg1::nil)))::nil
  else nil.  

Definition simplifyNatp (path: Path) (arg: Exp) : list Step :=
  match arg with
  | EApp (AOp1 Size arg1) =>
      (path, EApp (AFun "natp/size" (arg1::nil)))::nil
  | _ => nil
  end.

Definition simplifyAppNotQuoted (path: Path) (ap: Application Exp) : list Step :=
  match ap with
  | AOp1 op arg =>
    match op with
    | Atom => simplifyAtom path arg
    | Car => simplifyCar path arg
    | Cdr => simplifyCdr path arg
    | Natp => simplifyNatp path arg
    | _ => nil
    end
  | AOp2 op arg1 arg2 =>
    match op with
    | Equal => simplifyEqual path arg1 arg2
    | _ => nil
    end
  | _ => nil
  end.

Definition simplifyApp (path: Path) (ap: Application Exp) : list Step :=
  match ap with
  | AOp1 op (EQuote _) => (path, EApp ap)::nil
  | AOp2 op (EQuote _) (EQuote _) => (path, EApp ap)::nil
  | _ => simplifyAppNotQuoted path ap
  end.

Definition simplifyCurrent (fullscp: bool) (eRoot: Exp) (path: Path) (e0: Exp) : list Step :=
  match e0 with
  | EIf q a e => simplifyIf fullscp eRoot path q a e
  | EApp ap => simplifyApp path ap
  | _ => nil
  end.

(*
Fixpoint numberedListFrom {A} (from: nat) (xs: list A) {struct xs} : list (nat * A) :=
  match xs with
  | nil => nil
  | x::xs => (from, x) :: numberedListFrom (S from) xs
  end.

Fixpoint simplifyRec (eRoot: Exp) (path: Path) (e: Exp) {struct e} : option Step :=
  optPlus (simplifyCurrent eRoot path e)
  match e with
  | EIf q a e => 
      optPlus (simplifyRec eRoot (path ++ DirQ::nil) q)
        (optPlus (simplifyRec eRoot (path ++ DirA::nil) a)
          (simplifyRec eRoot (path ++ DirE::nil) e))
  | EApp (AOp1 op arg) => simplifyRec eRoot (path ++ DirNum 1::nil) arg
  | EApp (AOp2 op arg1 arg2) => 
      optPlus (simplifyRec eRoot (path ++ DirNum 1::nil) arg1)
        (simplifyRec eRoot (path ++ DirNum 2::nil) arg2)
  | EApp (AFun fname args) =>
      (fix F (nargs: list (nat * Exp)) {struct nargs} : option Step :=
        match nargs with
        | nil => None
        | (pos, arg)::nargs => 
            optPlus (simplifyRec eRoot (path ++ DirNum pos::nil) arg) (F nargs)
        end) (numberedListFrom 1 args)
  | _ => None
  end.
*)

Fixpoint simplifyRec (fullscp: bool) (eRoot: Exp) (path: Path) (e: Exp) : list Step :=
  listOptPlus 
    match e with
    | EIf q a e => 
        listOptPlus (simplifyRec fullscp eRoot (path ++ DirQ::nil) q)
          (listOptPlus (simplifyRec fullscp eRoot (path ++ DirA::nil) a)
            (simplifyRec fullscp eRoot (path ++ DirE::nil) e))
    | EApp (AOp1 op arg) => simplifyRec fullscp eRoot (path ++ DirNum 1::nil) arg
    | EApp (AOp2 op arg1 arg2) => 
        listOptPlus (simplifyRec fullscp eRoot (path ++ DirNum 1::nil) arg1)
          (simplifyRec fullscp eRoot (path ++ DirNum 2::nil) arg2)
    | EApp (AFun fname args) =>
        (fix F (pos: nat) (args: list Exp) {struct args} : list Step :=
          match args with
          | nil => nil
          | arg::args => listOptPlus (simplifyRec fullscp eRoot (path ++ DirNum pos::nil) arg)
                                     (F (S pos) args)
          end) 1 args
    | _ => nil
    end
    (simplifyCurrent fullscp eRoot path e).

Definition simplifyTop fullscp e := simplifyRec fullscp e nil e.

Definition rewriteOptSteps (defs: list Def) (e: Exp) (osteps: list Step)
  : option (list Step * Exp) :=
  match osteps with
  | nil => None
  | _::_ => Some (osteps, snd (rewriteSteps defs e osteps))
  end.

Fixpoint simplifyHelper (defs: list Def) (fullscp: bool) (fuel: nat)
  (prev: option (list Step * Exp)) : list Step :=
  match prev with
  | None => nil
  | Some (steps, e) =>
    match fuel with
    | 0 => nil
    | S fuel => steps ++ 
        simplifyHelper defs fullscp fuel
          (rewriteOptSteps defs e
            (simplifyTop fullscp e))
    end
  end.

Definition simplify (defs: list Def) (fullscp: bool) (fuel: nat) (e: Exp)
  : list Step :=
  simplifyHelper defs fullscp fuel
    (rewriteOptSteps defs e
      (simplifyTop fullscp e)).

(* ***** *)

Definition unfoldStepCurrent (defs: list Def) (path: Path) (e: Exp) : list Step :=
  match e with
  | EApp (AFun name args) => match lookup name defs with
    | Some (De Fun _ formals _) => 
        if eq_nat_dec (List.length args) (List.length formals)
        then (path, e)::nil 
        else nil
    | _ => nil
    end
  | _ => nil
  end.

Fixpoint unfoldSteps (defs: list Def) (path: Path) (e: Exp) : list Step :=
  unfoldStepCurrent defs path e 
  ++
  match e with
  | EIf q a e =>
      unfoldSteps defs (path ++ DirQ::nil) q
      ++ unfoldSteps defs (path ++ DirA::nil) a
      ++ unfoldSteps defs (path ++ DirE::nil) e
  | EApp ap => match ap with
    | AOp1 _ arg => unfoldSteps defs (path ++ DirNum 1 :: nil) arg
    | AOp2 _ arg1 arg2 => unfoldSteps defs (path ++ DirNum 1 :: nil) arg1
                       ++ unfoldSteps defs (path ++ DirNum 2 :: nil) arg2
    | AFun _ args =>
      (fix F (pos: nat) (args: list Exp) : list Step :=
        match args with
        | nil => nil
        | arg::args => unfoldSteps defs (path ++ DirNum pos :: nil) arg ++ F (S pos) args
        end) 1 args
    end
  | _ => nil
  end.

Definition unfoldStepsTop defs e := unfoldSteps defs nil e.

(* ***** *)

Definition applySimplify defs fullscp simplfuel e :=
  snd (rewriteSteps defs e (simplify defs fullscp simplfuel e)).

Definition bestUnfoldStepOfPair beststep_size (newstep: Step) newsize :=
  if (le_lt_dec (snd beststep_size) newsize)
  then (newstep, newsize)
  else beststep_size.

Fixpoint bestUnfoldStepRec (defs: list Def) (fullscp: bool) (simplfuel: nat)
  (e: Exp) (beststep_size: Step * nat) (steps: list Step) {struct steps} : Step :=
  match steps with
  | nil => fst beststep_size
  | step::steps =>
      bestUnfoldStepRec defs fullscp simplfuel e
        (bestUnfoldStepOfPair beststep_size step
          (expSize (applySimplify defs fullscp simplfuel (snd (rewriteStep defs e step)))))
        steps
  end.

Definition bestUnfoldStepAmong defs fullscp simplfuel e steps :=
  match steps with
  | nil => None
  | step::steps => 
      Some (bestUnfoldStepRec defs fullscp simplfuel e
        (step, expSize (applySimplify defs fullscp simplfuel (snd (rewriteStep defs e step))))
        steps)
  end.

Definition bestUnfoldStep defs fullscp simplfuel e :=
  bestUnfoldStepAmong defs fullscp simplfuel e (unfoldStepsTop defs e).

Definition chooseUnfoldStep defs fullscp (whitelist blacklist: list string) simplfuel e :=
  bestUnfoldStep defs fullscp simplfuel e.

(* ***** *)

Definition expWithSimplsteps defs fullscp simplfuel e :=
  (e, simplify defs fullscp simplfuel e).

Definition stepsWithResult defs e_steps :=
  (snd e_steps, snd (rewriteSteps defs (fst e_steps) (snd e_steps))).

Definition optunfoldWithPrevstepsResult defs fullscp whitelist blacklist simplfuel 
  (prevsteps_e: list Step * Exp) :=
  (fst prevsteps_e, snd prevsteps_e, 
    chooseUnfoldStep defs fullscp whitelist blacklist simplfuel (snd prevsteps_e)).

Fixpoint scpStepsRec (defs: list Def) (fullscp: bool) (unfoldfuel: nat) 
  (whitelist blacklist: list string)
  (simplfuel: nat) (simplsteps_e_optunfoldstep: list Step * Exp * option Step) 
  {struct unfoldfuel} : list Step :=
  match unfoldfuel with
  | 0 => fst (fst simplsteps_e_optunfoldstep)
  | S unfoldfuel => match snd simplsteps_e_optunfoldstep with
    | None => fst (fst simplsteps_e_optunfoldstep)
    | Some step => fst (fst simplsteps_e_optunfoldstep) ++
        step :: 
          (scpStepsRec defs fullscp unfoldfuel whitelist blacklist simplfuel
            (optunfoldWithPrevstepsResult defs fullscp whitelist blacklist simplfuel
              (stepsWithResult defs
                (expWithSimplsteps defs fullscp simplfuel
                  (snd (rewriteStep defs (snd (fst simplsteps_e_optunfoldstep)) step))))))
    end
  end.

Definition scpSteps defs fullscp unfoldfuel whitelist blacklist simplfuel e :=
  scpStepsRec defs fullscp unfoldfuel whitelist blacklist simplfuel
    (optunfoldWithPrevstepsResult defs fullscp whitelist blacklist simplfuel
      (stepsWithResult defs (expWithSimplsteps defs fullscp simplfuel e))).

(* ***** *)
(* ***** *)
(* ***** *)
(* Correctness proof for the prototype of a simple supercompiler generating J-Bob proofs *)

Lemma listOptPlus_nil_r: forall A (l: list A), listOptPlus l nil = l.
Proof.
  destruct l; auto.
Qed.

Lemma skipn_getFunArg: forall args pos arg args1,
  skipn pos args = arg::args1 -> getFunArg (S pos) args = Some arg.
Proof.
  induction args; destruct pos; simpl in *; try congruence; auto.
  intros. apply IHargs with (args1:=args1). assumption.
Qed.

Lemma le_length_getFunArg_Some: forall n args,
  S n <= List.length args -> exists e, getFunArg (S n) args = Some e.
Proof.
  intros n args. revert n. induction args.
  - simpl. intros. inversion H.
  - simpl. intros. apply le_S_n in H. destruct n.
    + exists a. reflexivity.
    + specialize (IHargs _ H). destruct IHargs as [e IH].
      rewrite IH. simpl. exists e. reflexivity.
Qed.

Lemma le_length_setFunArg_Some: forall n args e,
  S n <= List.length args -> exists args1, setFunArg (S n) args e = Some args1
    /\ List.length args = List.length args1.
Proof.
  intros n args. revert n. induction args.
  - simpl. intros. inversion H.
  - simpl. intros. apply le_S_n in H. destruct n.
    + exists (e::args). auto. 
    + specialize (IHargs _ e H). destruct IHargs as [args1 [IH1 IH2]].
      rewrite IH1. simpl. exists (a::args1). rewrite IH2. auto.
Qed.

Lemma getFunArg_setFunArg: forall args n e args1,
  setFunArg n args e = Some args1 -> getFunArg n args1 = Some e.
Proof.
  induction args; simpl; try congruence.
  destruct n; try congruence. destruct n.
  - intros. inversion H. subst. reflexivity.
  - intros. destruct (setFunArg (S n) args e) eqn: Heq; simpl in *; try congruence.
    inversion H. subst. simpl. apply IHargs; auto.
Qed.

Lemma setFunArg_composed_sameIndex: forall args n e1 e2 args1,
  setFunArg n args e1 = Some args1 ->
  setFunArg n args1 e2 = setFunArg n args e2.
Proof.
  induction args; simpl; try congruence.
  intros. destruct n; try congruence. destruct n.
  - inversion H. subst. reflexivity.
  - destruct (setFunArg (S n) args e1) eqn: Heq; simpl in *; try congruence.
    inversion H. subst. simpl. f_equal. apply IHargs with (e1:=e1). assumption.
Qed.

Lemma focusIsAtDirection_true_findFocusAtDirection_true: forall dir e,
  focusIsAtDirection dir e = true ->
  fst (findFocusAtDirection dir (true, e)) = true.
Proof.
  destruct dir; destruct e; simpl in *; try congruence.
  destruct a.
  - intros. repeat (destruct n; try congruence); auto.
  - intros. repeat (destruct n; try congruence); auto.
  - intros. destruct n; try congruence.
    destruct (le_lt_dec (S n) (Datatypes.length args)) as [Hle | Hgt]; try congruence.
    unfold getArg. 
    pose (H' := le_length_getFunArg_Some _ Hle).
    destruct H' as [e H']. rewrite H'. reflexivity.
Qed.

Lemma focusIsAtDirection_true_rewriteFocusAtDirection_true: forall dir e1 e2,
  focusIsAtDirection dir e1 = true ->
  fst (rewriteFocusAtDirection dir (true, e1) (true, e2)) = true.
Proof.
  destruct dir; destruct e1; simpl in *; try congruence.
  destruct a.
  - intros. repeat (destruct n; try congruence); auto.
  - intros. repeat (destruct n; try congruence); auto.
  - intros. destruct n; try congruence.
    destruct (le_lt_dec (S n) (Datatypes.length args)) as [Hle | Hgt]; try congruence.
    unfold rewriteFocusAtDirection. simpl. 
    pose (H' := le_length_setFunArg_Some _ e2 Hle).
    destruct H' as [e [Hsa1 Hsa2]]. rewrite Hsa1. reflexivity.
Qed.

Lemma focusIsAtDirection_rewriteFocusAtDirection: forall dir e1 e2,
  focusIsAtDirection dir e1 = true ->
  focusIsAtDirection dir (snd (rewriteFocusAtDirection dir (true, e1) (true, e2))) = true.
Proof.
  destruct dir; destruct e1; simpl in *; try congruence.
  destruct a.
  - destruct n; try congruence. destruct n; try congruence. auto.
  - destruct n; try congruence. destruct n; auto. destruct n; try congruence. auto. 
  - destruct n; try congruence. 
    destruct (le_lt_dec (S n) (Datatypes.length args)) as [Hle | Hgt]; try congruence.
    intros.
    unfold rewriteFocusAtDirection. cbv [fst snd andb]. unfold setArg.
    pose (Hsa := le_length_setFunArg_Some _ e2 Hle).
    destruct Hsa as [args1 [Hsa1 Hsa2]]. rewrite Hsa1.
    remember (S n) as n1. simpl. rewrite <- Hsa2. subst.
    destruct (le_lt_dec (S n) (Datatypes.length args)); auto.
    contradict Hle. auto with arith.
Qed.

Lemma findFocusAtDirection_rewriteFocusAtDirection: forall dir e1 e2,
  focusIsAtDirection dir e1 = true ->
  findFocusAtDirection dir (true, snd (rewriteFocusAtDirection dir (true, e1) (true, e2))) 
  = (true, e2).
Proof.
  destruct dir; destruct e1; simpl in *; try congruence.
  destruct a.
  - destruct n; try congruence. destruct n; try congruence. auto.
  - destruct n; try congruence. destruct n; auto. destruct n; try congruence. auto. 
  - destruct n; try congruence. 
    destruct (le_lt_dec (S n) (Datatypes.length args)) as [Hle | Hgt]; try congruence.
    intros.
    unfold rewriteFocusAtDirection. cbv [fst snd andb]. unfold setArg.
    pose (Hsa := le_length_setFunArg_Some _ e2 Hle).
    destruct Hsa as [args1 [Hsa1 Hsa2]]. rewrite Hsa1.
    remember (S n) as n1. simpl.
    rewrite getFunArg_setFunArg with (args:=args)(e:=e2); auto.
Qed.

Lemma rewriteFocusAtDirection_composed_sameDirection: forall dir e1 e2 e3,
  focusIsAtDirection dir e1 = true ->
  rewriteFocusAtDirection dir
    (true, snd (rewriteFocusAtDirection dir (true, e1) (true, e2))) (true, e3)
  = rewriteFocusAtDirection dir (true, e1) (true, e3).
Proof.
  destruct dir; destruct e1; simpl; try congruence; auto.
  destruct a.
  - destruct n; try congruence. destruct n; try congruence. auto.
  - destruct n; try congruence. destruct n; auto. destruct n; auto.
  - destruct n; try congruence.
    destruct (le_lt_dec (S n) (Datatypes.length args)) as [Hle | Hgt]; try congruence.
    intros. unfold rewriteFocusAtDirection. simpl.
    pose (Hsa := le_length_setFunArg_Some _ e2 Hle).
    destruct Hsa as [args1 [Hsa1 Hsa2]]. rewrite Hsa1.
    simpl.
    rewrite setFunArg_composed_sameIndex with (args:=args)(e1:=e2); auto.
    pose (Hsa := le_length_setFunArg_Some _ e3 Hle).
    destruct Hsa as [args' [Hsa1' Hsa2']]. rewrite Hsa1'. reflexivity.
Qed.

Lemma focusIsAtPath_app: forall path1 path2 e,
  focusIsAtPath (path1 ++ path2) e = 
  andb (focusIsAtPath path1 e) (focusIsAtPath path2 (findFocusAtPath path1 e)).
Proof.
  induction path1; auto.
  simpl. intros. destruct (focusIsAtDirection a e); auto.
Qed.

Lemma findFocusAtPath_app: forall path1 path2 e,
  focusIsAtPath path1 e = true ->
  findFocusAtPath (path1 ++ path2) e
  = findFocusAtPath path2 (findFocusAtPath path1 e).
Proof.
  induction path1; auto.
  simpl. intros. destruct (focusIsAtDirection a e); try congruence.
  apply IHpath1; auto.
Qed.

Lemma focusIsAtPath_true_rewriteFocusAtPath_true: forall path e e2,
  focusIsAtPath path e = true ->
  fst (rewriteFocusAtPath path (true, e) (true, e2)) = true.
Proof.
  induction path; auto.
  simpl. intros. 
  destruct (focusIsAtDirection a e) eqn: Heq; try congruence.
  unfold rewriteFocusAtDirection. simpl.
  pattern (findFocusAtDirection a (true, e)).
  rewrite surjective_pairing.
  rewrite focusIsAtDirection_true_findFocusAtDirection_true; auto.
  rewrite IHpath; auto.
  destruct a; destruct e; try solve [simpl in *; congruence].
  simpl in *.
  destruct a.
  - repeat (destruct n; try congruence). reflexivity.
  - repeat (destruct n; try congruence); auto.
  - destruct n; try congruence.
    destruct (le_lt_dec (S n) (Datatypes.length args)) as [Hle | Hgt]; try congruence.
    unfold setArg.
    match goal with [|- context[setFunArg _ _ ?E]] 
      => pose (HsetFArg := le_length_setFunArg_Some _ E Hle)
    end.
    destruct HsetFArg as [args1 [Hsa1 Hsa2]].
    rewrite Hsa1. reflexivity.
Qed.
    
Lemma rewriteFocusAtPath_app: forall path1 path2 e1 e2,
  focusIsAtPath (path1 ++ path2) e1 = true ->
  rewriteFocusAtPath (path1 ++ path2) (true, e1) (true, e2) =
  rewriteFocusAtPath path1 (true, e1)
   (rewriteFocusAtPath path2 (true, findFocusAtPath path1 e1) (true, e2)).
Proof.
  induction path1; auto.
  - simpl. intros.
    rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
  - simpl. intros. rename a into dir.
    destruct (focusIsAtDirection dir e1) eqn: Hfid; try congruence.
    rewrite focusIsAtPath_app in H.
    rewrite Bool.andb_true_iff in H.
    destruct H as [Hfi1 Hfi2].
    rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
    f_equal. 
    rewrite surjective_pairing with (p:= findFocusAtDirection dir (true, e1)).
    rewrite focusIsAtDirection_true_findFocusAtDirection_true; auto.
    apply IHpath1.
    simpl.
    rewrite focusIsAtPath_app; auto.
    rewrite Hfi1. rewrite Hfi2. reflexivity.
Qed.

Lemma focusIsAtPath_rewriteFocusAtPath: forall path e1 e2,
  focusIsAtPath path e1 = true ->
  focusIsAtPath path (snd (rewriteFocusAtPath path (true, e1) (true, e2))) = true.
Proof.
  induction path; auto.
  simpl. intros.
  destruct (focusIsAtDirection a e1) eqn: Heq; try congruence.
  rewrite surjective_pairing with (p:= findFocusAtDirection a (true, e1)).
  rewrite focusIsAtDirection_true_findFocusAtDirection_true; auto.
  rewrite surjective_pairing with (p:= 
            (rewriteFocusAtPath path
               (true, snd (findFocusAtDirection a (true, e1))) (true, e2))).
  rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
  rewrite focusIsAtDirection_rewriteFocusAtDirection; auto.
  rewrite findFocusAtDirection_rewriteFocusAtDirection; auto.
  simpl. apply IHpath; auto.
Qed.

Lemma findFocusAtPath_rewriteFocusAtPath: forall path e1 e2,
  focusIsAtPath path e1 = true ->
  findFocusAtPath path (snd (rewriteFocusAtPath path (true, e1) (true, e2))) = e2.
Proof.
  induction path; auto.
  simpl. intros.
  destruct (focusIsAtDirection a e1) eqn: Heq; try congruence.
  rewrite surjective_pairing with (p:= findFocusAtDirection a (true, e1)).
  rewrite focusIsAtDirection_true_findFocusAtDirection_true; auto.
  rewrite surjective_pairing with (p:= 
            (rewriteFocusAtPath path
               (true, snd (findFocusAtDirection a (true, e1))) (true, e2))).
  rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
  rewrite findFocusAtDirection_rewriteFocusAtDirection; auto.
  simpl. apply IHpath; auto.
Qed.

Lemma rewriteFocusAtPath_composed_samePath: forall path e1 e2 e3,
  focusIsAtPath path e1 = true ->
  rewriteFocusAtPath path
    (true, snd (rewriteFocusAtPath path (true, e1) (true, e2))) (true, e3)
  = rewriteFocusAtPath path (true, e1) (true, e3).
Proof.
  induction path; auto.
  rename a into dir.
  simpl. intros.
  destruct (focusIsAtDirection dir e1) eqn: Hfid; try congruence.
  rewrite surjective_pairing with (p := findFocusAtDirection dir (true, e1)).
  rewrite focusIsAtDirection_true_findFocusAtDirection_true; auto.
  rewrite surjective_pairing with (p :=
    (rewriteFocusAtPath path (true, snd (findFocusAtDirection dir (true, e1)))
                (true, e2))).
  rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
  rewrite findFocusAtDirection_rewriteFocusAtDirection; auto.
  rewrite IHpath; auto.
  rewrite surjective_pairing with (p :=
    (rewriteFocusAtPath path (true, snd (findFocusAtDirection dir (true, e1)))
                (true, e3))).
  rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
  rewrite rewriteFocusAtDirection_composed_sameDirection; auto.
Qed.

Lemma premA_app: forall path1 path2 q eRoot,
  premA q (path1 ++ path2) eRoot = 
  orb (premA q path1 eRoot) (premA q path2 (findFocusAtPath path1 eRoot)).
Proof.
  induction path1; auto.
  rename a into dir.
  simpl. intros. destruct dir; auto.
  destruct eRoot; auto.
  destruct (expEqb eRoot1 q); auto.
Qed.

Lemma premE_app: forall path1 path2 q eRoot,
  premE q (path1 ++ path2) eRoot = 
  orb (premE q path1 eRoot) (premE q path2 (findFocusAtPath path1 eRoot)).
Proof.
  induction path1; auto.
  rename a into dir.
  simpl. intros. destruct dir; auto.
  destruct eRoot; auto.
  destruct (expEqb eRoot1 q); auto.
Qed.

Lemma premA_rewriteFocusAtPath_samePath: forall q path e1 e2,
  focusIsAtPath path e1 = true ->
  premA q path (snd (rewriteFocusAtPath path (true, e1) (true, e2))) = premA q path e1.
Proof.
  induction path; auto.
  simpl. intros. rename a into dir.
  destruct (focusIsAtDirection dir e1) eqn: Hfid; try congruence.
  destruct dir.
  - rewrite surjective_pairing with (p:=findFocusAtDirection DirQ (true, e1)).
    rewrite focusIsAtDirection_true_findFocusAtDirection_true; auto.
    rewrite surjective_pairing with (p:=
      rewriteFocusAtPath path (true, snd (findFocusAtDirection DirQ (true, e1))) (true, e2)).
    rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
    rewrite findFocusAtDirection_rewriteFocusAtDirection; auto.
    remember DirQ as dir. simpl. apply IHpath; auto.
  - destruct e1; simpl in *; try congruence.
    unfold rewriteFocusAtDirection. simpl.
    rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
    simpl.
    destruct (expEqb e1_1 q); auto.
  - rewrite surjective_pairing with (p:=findFocusAtDirection DirE (true, e1)).
    rewrite focusIsAtDirection_true_findFocusAtDirection_true; auto.
    rewrite surjective_pairing with (p:=
      rewriteFocusAtPath path (true, snd (findFocusAtDirection DirE (true, e1))) (true, e2)).
    rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
    rewrite findFocusAtDirection_rewriteFocusAtDirection; auto.
    remember DirE as dir. simpl. apply IHpath; auto.
  - remember (DirNum n) as dir.
    rewrite surjective_pairing with (p:=findFocusAtDirection dir (true, e1)).
    rewrite focusIsAtDirection_true_findFocusAtDirection_true; auto.
    rewrite surjective_pairing with (p:=
      rewriteFocusAtPath path (true, snd (findFocusAtDirection dir (true, e1))) (true, e2)).
    rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
    rewrite findFocusAtDirection_rewriteFocusAtDirection; auto.
    simpl. apply IHpath; auto.
Qed.

Lemma rewriteContinue_true_newStep_true: forall defs steps e1 b_e2,
  fst (rewriteContinue defs steps e1 b_e2) = true -> fst b_e2 = true.
Proof.
  destruct b_e2 as [b e2]. destruct steps; destruct b; simpl in *; try congruence.
Qed.

Lemma rewriteContinue_app: forall defs steps1 steps2 e1 e2,
  fst (rewriteContinue defs steps1 e1 (true, e2)) = true ->
  rewriteContinue defs (steps1 ++ steps2) e1 (true, e2) =
  match steps2 with
  | nil => rewriteContinue defs steps1 e1 (true, e2)
  | step2::steps2 => 
      let e3 := snd (rewriteContinue defs steps1 e1 (true, e2)) in
      rewriteContinue defs steps2 e3 (rewriteStep defs e3 step2)
  end.
Proof.
  induction steps1 as [|step1 steps1]; destruct steps2 as [|step2 steps2]; auto.
  - simpl. intros. rewrite <- app_nil_end. reflexivity.
  - simpl. intros.
    pose (H' := rewriteContinue_true_newStep_true _ _ _ _ H).
    rewrite surjective_pairing with (p:=rewriteStep defs e2 step1).
    rewrite H'.
    rewrite IHsteps1; auto.
    rewrite <- H'. rewrite <- surjective_pairing. congruence.
Qed.

Lemma rewriteSteps_app: forall defs steps1 steps2 e,
  fst (rewriteSteps defs e steps1) = true ->
  rewriteSteps defs e (steps1 ++ steps2) =
  rewriteSteps defs (snd (rewriteSteps defs e steps1)) steps2.
Proof.
  destruct steps1; auto.
  simpl. intros.
  pose (H' := rewriteContinue_true_newStep_true _ _ _ _ H).
  rewrite surjective_pairing with (p:=rewriteStep defs e s).
  rewrite H'. rewrite rewriteContinue_app.
  - destruct steps2; auto.
    simpl. rewrite <- H' at 3. rewrite <- surjective_pairing.
    rewrite <- H at 2. rewrite <- surjective_pairing.
    rewrite <- H'. rewrite <- surjective_pairing. reflexivity.
  - rewrite <- H'. rewrite <- surjective_pairing. congruence.
Qed.

Fixpoint defsSubset (defs1 defs2: list Def) : bool :=
  match defs1 with
  | nil => true
  | De _ name _ _ as def1::defs1 => 
    match lookup name defs2 with
    | None => false
    | Some def2 => 
        if Def_eq_dec def1 def2 
        then defsSubset defs1 defs2
        else false
    end
  end.

Lemma defsSubset_lookup_Some: forall defs1 defs2,
  defsSubset defs1 defs2 = true -> 
  forall name, lookup name defs1 <> None
  -> lookup name defs2 = lookup name defs1.
Proof.
  induction defs1; simpl; try congruence.
  intros. destruct a as [kind name' formals body].
  destruct (string_dec name name') as [Heq | Hneq].
  - subst. destruct (lookup name' defs2); try congruence.
    destruct (Def_eq_dec (De kind name' formals body) d); congruence.
  - apply IHdefs1; auto.
    destruct (lookup name' defs2); try congruence.
    destruct (Def_eq_dec (De kind name' formals body) d); congruence.
Qed.

Lemma expSize_neq_Exp_neq: forall e1 e2, expSize e1 <> expSize e2 -> e1 <> e2.
Proof.
  intros. intro Hcontra. subst. congruence.
Qed.

(* ***** *)

Lemma tryIfLifting_correct: forall defs eRoot path dir q a e,
  defsSubset axioms defs = true ->
  focusIsAtPath (path ++ dir::nil) eRoot = true -> 
  findFocusAtPath (path ++ dir::nil) eRoot = EIf q a e -> 
  premA q path eRoot = false ->
  fst (rewriteSteps defs eRoot (tryIfLifting eRoot path dir q a e)) = true.
Proof.
  intros ? ? ? ? ? ? ? Hdefs Hfi Hff HpremA. unfold tryIfLifting.
  destruct (contextGoodForIfLifting dir) eqn: Heq; auto.
  unfold ifLiftingSteps. 
  remember (path ++ DirE::dir::nil, EApp (AFun "if-nest-E" (q::a::e::nil))) as step3.
  remember (path ++ DirA::dir::nil, EApp (AFun "if-nest-A" (q::a::e::nil))) as step2.
  remember (step2::step3::nil) as steps23.
  simpl. unfold rewriteStep. simpl.
  rewrite defsSubset_lookup_Some with (defs1:=axioms); auto.
  2: simpl; congruence. simpl.
  unfold equalityPath. simpl.
  rewrite focusIsAtPath_app in Hfi. rewrite Bool.andb_true_iff in Hfi.
  destruct Hfi as [Hfi1 Hfi2]. simpl in Hfi2.
  destruct (focusIsAtDirection dir (findFocusAtPath path eRoot)) eqn: Hfi3; try congruence.
  clear Hfi2.
  rewrite Hfi1. 
  unfold equality. rewrite expEqb_refl. 
  destruct (expEqb (findFocusAtPath path eRoot)   
    (EIf q (findFocusAtPath path eRoot) (findFocusAtPath path eRoot))) eqn: Heq1.
  - apply expEqb_true_eq in Heq1.
    contradict Heq1.
    apply expSize_neq_Exp_neq.
    simpl. intro Hcontra. rewrite plus_comm in Hcontra.
    rewrite plus_n_Sm in Hcontra. apply plus_minus in Hcontra.
    rewrite minus_diag in Hcontra.
    congruence.
  - subst steps23 step2. remember (step3::nil) as steps3.
    simpl. rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
    unfold rewriteStep. simpl.
    rewrite defsSubset_lookup_Some with (defs1:=axioms); auto.
    2: simpl; congruence. simpl.
    unfold equalityPath. simpl.
    rewrite focusIsAtPath_app. 
    rewrite focusIsAtPath_rewriteFocusAtPath; auto. simpl.
    rewrite findFocusAtPath_rewriteFocusAtPath; auto. 
    simpl. rewrite Hfi3.
    rewrite premA_app.
    rewrite findFocusAtPath_rewriteFocusAtPath; auto.
    simpl. rewrite expEqb_refl. rewrite Bool.orb_true_r.
    rewrite findFocusAtPath_app.
    2: apply focusIsAtPath_rewriteFocusAtPath; auto.
    rewrite findFocusAtPath_rewriteFocusAtPath; auto.
    simpl.
    unfold equality. 
    rewrite findFocusAtPath_app in Hff; auto. simpl in Hff.
    rewrite Hff. rewrite expEqb_refl.
    rewrite rewriteFocusAtPath_app; auto.
    2: rewrite focusIsAtPath_app; simpl.
    2: rewrite focusIsAtPath_rewriteFocusAtPath; auto.
    2: rewrite findFocusAtPath_rewriteFocusAtPath; auto.
    2: simpl; rewrite Hfi3; auto.
    rewrite findFocusAtPath_rewriteFocusAtPath; auto.
    simpl.
    rewrite focusIsAtDirection_true_findFocusAtDirection_true; auto.
    simpl. unfold rewriteFocusAtDirection at 1. simpl.
    rewrite focusIsAtDirection_true_rewriteFocusAtDirection_true; auto.
    rewrite rewriteFocusAtPath_composed_samePath; auto.
    subst. simpl.
    rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
    unfold rewriteStep. simpl.
    rewrite defsSubset_lookup_Some with (defs1:=axioms); auto.
    2: simpl; congruence. simpl.
    rewrite ifSame.
    unfold equalityPath. simpl.
    rewrite focusIsAtPath_app. 
    rewrite focusIsAtPath_rewriteFocusAtPath; auto. simpl.
    rewrite findFocusAtPath_rewriteFocusAtPath; auto. 
    simpl. rewrite Hfi3.
    rewrite premA_app.
    rewrite findFocusAtPath_rewriteFocusAtPath; auto.
    rewrite premA_rewriteFocusAtPath_samePath; auto.
    rewrite HpremA. 
    assert (H: premA q (DirE :: dir :: nil)
                  (EIf q
                     (snd
                        (rewriteFocusAtDirection dir
                           (true, findFocusAtPath path eRoot) (true, a)))
                     (findFocusAtPath path eRoot)) = false).
    { simpl. destruct dir; simpl in Heq; try congruence. }
    rewrite H. simpl.
    rewrite premE_app.
    rewrite findFocusAtPath_rewriteFocusAtPath; auto.
    simpl. rewrite expEqb_refl. rewrite Bool.orb_true_r.
    rewrite findFocusAtPath_app.
    2: apply focusIsAtPath_rewriteFocusAtPath; auto.
    rewrite findFocusAtPath_rewriteFocusAtPath; auto.
    simpl.
    unfold equality. 
    rewrite Hff. rewrite expEqb_refl.
    rewrite rewriteFocusAtPath_app; auto.
    2: rewrite focusIsAtPath_app; simpl.
    2: rewrite focusIsAtPath_rewriteFocusAtPath; auto.
    2: rewrite findFocusAtPath_rewriteFocusAtPath; auto.
    2: simpl; rewrite Hfi3; auto.
    rewrite findFocusAtPath_rewriteFocusAtPath; auto.
    simpl.
    rewrite focusIsAtDirection_true_findFocusAtDirection_true; auto.
    simpl. unfold rewriteFocusAtDirection at 2. simpl.
    rewrite focusIsAtDirection_true_rewriteFocusAtDirection_true; auto.
    rewrite rewriteFocusAtPath_composed_samePath; auto.
    apply focusIsAtPath_true_rewriteFocusAtPath_true; auto.
Qed.

Lemma simplifyIf_correct: forall defs fullscp eRoot path q a e,
  defsSubset axioms defs = true ->
  focusIsAtPath path eRoot = true -> findFocusAtPath path eRoot = EIf q a e ->
  fst (rewriteSteps defs eRoot (simplifyIf fullscp eRoot path q a e)) = true.
Proof.
  intros ? ? ? ? ? ? ? Hdefs Hfi Hff.
  unfold simplifyIf.
  destruct (expEqb q (EQuote (VSym "t"))) eqn: Heq1.
  { apply expEqb_true_eq in Heq1. subst. 
    simpl. unfold rewriteStep. simpl.
    rewrite defsSubset_lookup_Some with (defs1:=axioms); auto.
    2: simpl; congruence. simpl.
    unfold equalityPath. simpl.
    rewrite Hfi. rewrite Hff.
    unfold equality. rewrite expEqb_refl. 
    rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
    apply focusIsAtPath_true_rewriteFocusAtPath_true; auto.
  }
  {
    destruct (expEqb q (EQuote (VSym "nil"))) eqn: Heq2.
    { apply expEqb_true_eq in Heq2. subst. 
      simpl. unfold rewriteStep. simpl.
      rewrite defsSubset_lookup_Some with (defs1:=axioms); auto.
      2: simpl; congruence. simpl.
      unfold equalityPath. simpl.
      rewrite Hfi. rewrite Hff.
      unfold equality. rewrite expEqb_refl. 
      rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
      apply focusIsAtPath_true_rewriteFocusAtPath_true; auto.
    }
    { destruct fullscp; auto.
      destruct (premA q path eRoot) eqn: Heq3.
      { simpl. unfold rewriteStep. simpl.
        rewrite defsSubset_lookup_Some with (defs1:=axioms); auto.
        2: simpl; congruence. simpl.
        unfold equalityPath. simpl.
        rewrite Hfi. rewrite Hff. rewrite Heq3. simpl.
        unfold equality. rewrite expEqb_refl. 
        rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
        apply focusIsAtPath_true_rewriteFocusAtPath_true; auto.
      }
      { destruct (premE q path eRoot) eqn: Heq4.
        { simpl. unfold rewriteStep. simpl.
          rewrite defsSubset_lookup_Some with (defs1:=axioms); auto.
          2: simpl; congruence. simpl.
          unfold equalityPath. simpl.
          rewrite Hfi. rewrite Hff. rewrite Heq3. rewrite Heq4. simpl.
          unfold equality. rewrite expEqb_refl. 
          rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
          apply focusIsAtPath_true_rewriteFocusAtPath_true; auto.
        }
        { destruct (expEqb a e) eqn: Heq5.
          { simpl. unfold rewriteStep. simpl.
            rewrite defsSubset_lookup_Some with (defs1:=axioms); auto.
            2: simpl; congruence. simpl.
            unfold equalityPath. simpl.
            rewrite Hfi. rewrite Hff.
            apply expEqb_true_eq in Heq5. subst.
            unfold equality. rewrite expEqb_refl. 
            rewrite focusIsAtPath_true_rewriteFocusAtPath_true; auto.
            apply focusIsAtPath_true_rewriteFocusAtPath_true; auto.
          }
          { destruct path; auto.
            apply tryIfLifting_correct; auto.
            - rewrite <- app_removelast_last; auto. congruence.
            - rewrite <- app_removelast_last; auto. congruence.
            - rewrite app_removelast_last with (l:=d::path)(d:=DirA) in Heq3; auto. 
              2: congruence.
              rewrite premA_app in Heq3. rewrite Bool.orb_false_iff in Heq3.
              destruct Heq3 as [Heq3_1 Heq3_2]. assumption.
          }
        }
      }
    }
  }
Qed.

Lemma simplifyAppNotQuoted_correct: forall defs eRoot path ap,
  defsSubset axioms defs = true ->
  focusIsAtPath path eRoot = true -> findFocusAtPath path eRoot = EApp ap ->
  fst (rewriteSteps defs eRoot (simplifyAppNotQuoted path ap)) = true.
Proof.
  intros ? ? ? ? Hdefs Hfi Hff. 
  unfold simplifyAppNotQuoted. destruct ap; auto.
  - destruct op; auto.
    + unfold simplifyCar. destruct arg; auto. destruct a; auto. destruct op; auto.
      simpl. rewrite ifSame. unfold rewriteStep. simpl.
      rewrite defsSubset_lookup_Some with (defs1:=axioms); auto.
      2: simpl; congruence. simpl.
      unfold equalityPath. rewrite Hfi. simpl. rewrite Hff.
      unfold equality. rewrite expEqb_refl.
      apply focusIsAtPath_true_rewriteFocusAtPath_true; auto.
    + unfold simplifyCdr. destruct arg; auto. destruct a; auto. destruct op; auto.
      simpl. rewrite ifSame. unfold rewriteStep. simpl.
      rewrite defsSubset_lookup_Some with (defs1:=axioms); auto.
      2: simpl; congruence. simpl.
      unfold equalityPath. rewrite Hfi. simpl. rewrite Hff.
      unfold equality. rewrite expEqb_refl.
      apply focusIsAtPath_true_rewriteFocusAtPath_true; auto.
    + unfold simplifyAtom. destruct arg; auto. destruct a; auto. destruct op; auto.
      simpl. rewrite ifSame. unfold rewriteStep. simpl.
      rewrite defsSubset_lookup_Some with (defs1:=axioms); auto.
      2: simpl; congruence. simpl.
      unfold equalityPath. rewrite Hfi. simpl. rewrite Hff.
      unfold equality. rewrite expEqb_refl.
      apply focusIsAtPath_true_rewriteFocusAtPath_true; auto.
    + unfold simplifyNatp. destruct arg; auto. destruct a; auto. destruct op; auto.
      simpl. rewrite ifSame. unfold rewriteStep. simpl.
      rewrite defsSubset_lookup_Some with (defs1:=axioms); auto.
      2: simpl; congruence. simpl.
      unfold equalityPath. rewrite Hfi. simpl. rewrite Hff.
      unfold equality. rewrite expEqb_refl.
      apply focusIsAtPath_true_rewriteFocusAtPath_true; auto.
  - destruct op; auto.
    unfold simplifyEqual. destruct (expEqb arg1 arg2) eqn: Heq; auto.
    apply expEqb_true_eq in Heq. subst.
    simpl. rewrite ifSame. unfold rewriteStep. simpl.
    rewrite defsSubset_lookup_Some with (defs1:=axioms); auto.
    2: simpl; congruence. simpl.
    unfold equalityPath. rewrite Hfi. simpl. rewrite Hff.
    unfold equality. rewrite expEqb_refl.
    apply focusIsAtPath_true_rewriteFocusAtPath_true; auto.
Qed.

Lemma simplifyApp_correct: forall defs eRoot path ap,
  defsSubset axioms defs = true ->
  focusIsAtPath path eRoot = true -> findFocusAtPath path eRoot = EApp ap ->
  fst (rewriteSteps defs eRoot (simplifyApp path ap)) = true.
Proof.
  intros.
  unfold simplifyApp.
  destruct ap; try solve [apply simplifyAppNotQuoted_correct; auto].
  - destruct arg; try solve [apply simplifyAppNotQuoted_correct; auto].
    simpl. rewrite ifSame. unfold rewriteStep. simpl.
    unfold equalityPath. rewrite H0. simpl. rewrite H1.
    unfold equality. rewrite expEqb_refl.
    apply focusIsAtPath_true_rewriteFocusAtPath_true; auto.
  - destruct arg1; destruct arg2; try solve [apply simplifyAppNotQuoted_correct; auto].
    simpl. rewrite ifSame. unfold rewriteStep. simpl.
    unfold equalityPath. rewrite H0. simpl. rewrite H1.
    unfold equality. rewrite expEqb_refl.
    apply focusIsAtPath_true_rewriteFocusAtPath_true; auto.
Qed.

Lemma simplifyCurrent_correct: forall defs fullscp eRoot path e,
  defsSubset axioms defs = true ->
  focusIsAtPath path eRoot = true -> findFocusAtPath path eRoot = e ->
  fst (rewriteSteps defs eRoot (simplifyCurrent fullscp eRoot path e)) = true.
Proof.
  intros ? ? ? ? ? Hdefs Hfi Hff. 
  destruct e; try solve [simpl; congruence].
  - unfold simplifyCurrent.
    apply simplifyIf_correct; auto.
  - unfold simplifyCurrent.
    apply simplifyApp_correct; auto.
Qed. 

(* Print Assumptions simplifyCurrent_correct. *)

(* ***** *)

(*
Lemma simplifyRec_Some_stepProgress_helper1: 
  forall eRoot path1 path2 path3 e1 e2 step,
  focusIsAtPath path1 eRoot = true ->
  focusIsAtPath path2 (findFocusAtPath path1 eRoot) = true ->
  focusIsAtPath path3 (findFocusAtPath path1 eRoot) = true ->
  findFocusAtPath path2 (findFocusAtPath path1 eRoot) = e1 ->
  findFocusAtPath path3 (findFocusAtPath path1 eRoot) = e2 ->
  (forall path step, focusIsAtPath path eRoot = true ->
    findFocusAtPath path eRoot = e1 ->
    simplifyRec eRoot path e1 = Some step ->
    fst (rewriteStep axioms eRoot step) = true) ->
  (forall path step, focusIsAtPath path eRoot = true ->
    findFocusAtPath path eRoot = e2 ->
    simplifyRec eRoot path e2 = Some step ->
    fst (rewriteStep axioms eRoot step) = true) ->
  optPlus (simplifyRec eRoot (path1 ++ path2) e1)
    (simplifyRec eRoot (path1 ++ path3) e2) = Some step ->
  fst (rewriteStep axioms eRoot step) = true.
Proof.
  intros ? ? ? ? ? ? ? HisF1 HisF2 HisF3 Hff1 Hff2 IH1 IH2 Hoplus.
  destruct (simplifyRec eRoot (path1 ++ path2) e1) eqn: Hsr1;
  destruct (simplifyRec eRoot (path1 ++ path3) e2) eqn: Hsr2;
  simpl in *; try congruence.
  - apply IH1 with (path := path1 ++ path2); try congruence.
    + rewrite focusIsAtPath_app.
      rewrite HisF1. rewrite HisF2. reflexivity.
    + rewrite findFocusAtPath_app; auto. 
  - apply IH1 with (path := path1 ++ path2); try congruence.
    + rewrite focusIsAtPath_app.
      rewrite HisF1. rewrite HisF2. reflexivity.
    + rewrite findFocusAtPath_app; auto. 
  - apply IH2 with (path := path1 ++ path3); try congruence.
    + rewrite focusIsAtPath_app.
      rewrite HisF1. rewrite HisF3. reflexivity.
    + rewrite findFocusAtPath_app; auto. 
Qed.
*)

Lemma simplifyRec_correct: forall defs fullscp eRoot path e,
  defsSubset axioms defs = true ->
  focusIsAtPath path eRoot = true -> findFocusAtPath path eRoot = e ->
  fst (rewriteSteps defs eRoot (simplifyRec fullscp eRoot path e)) = true.
Proof.
  intros ? ? ? ? ? Hdefs Hfi Hff. revert path Hfi Hff.
  induction e using Exp_fullInd; try solve [simpl; congruence].
  - intros. unfold simplifyRec. fold simplifyRec.
    destruct (listOptPlus (simplifyRec fullscp eRoot (path ++ DirQ :: nil) e1)
           (listOptPlus (simplifyRec fullscp eRoot (path ++ DirA :: nil) e2)
              (simplifyRec fullscp eRoot (path ++ DirE :: nil) e3))) eqn: Hreceq.
    + simpl. apply simplifyIf_correct; auto.
    + destruct (simplifyRec fullscp eRoot (path ++ DirQ::nil) e1) eqn: Hsr1.
      { simpl in Hreceq.
        destruct (simplifyRec fullscp eRoot (path ++ DirA::nil) e2) eqn: Hsr2.
        { simpl in Hreceq. unfold listOptPlus. rewrite <- Hreceq.
          apply IHe3.
          - rewrite focusIsAtPath_app. rewrite Hfi. rewrite Hff. reflexivity.
          - rewrite findFocusAtPath_app; auto. rewrite Hff. reflexivity. }
        { simpl in Hreceq. unfold listOptPlus. rewrite Hreceq in Hsr2. rewrite <- Hsr2.
          apply IHe2.
          - rewrite focusIsAtPath_app. rewrite Hfi. rewrite Hff. reflexivity.
          - rewrite findFocusAtPath_app; auto. rewrite Hff. reflexivity. }
      }
      { simpl in Hreceq. unfold listOptPlus. rewrite Hreceq in Hsr1. rewrite <- Hsr1.
          apply IHe1.
          - rewrite focusIsAtPath_app. rewrite Hfi. rewrite Hff. reflexivity.
          - rewrite findFocusAtPath_app; auto. rewrite Hff. reflexivity. }
  - intros. unfold simplifyRec. fold simplifyRec.
    destruct (simplifyRec fullscp eRoot (path ++ DirNum 1 :: nil) e) eqn: Hsr.
    { unfold listOptPlus. apply simplifyCurrent_correct; auto. }
    { unfold listOptPlus. rewrite <- Hsr. apply IHe.
      - rewrite focusIsAtPath_app. rewrite Hfi. rewrite Hff. reflexivity.
      - rewrite findFocusAtPath_app; auto. rewrite Hff. reflexivity. }
  - intros. unfold simplifyRec. fold simplifyRec.
    destruct (listOptPlus (simplifyRec fullscp eRoot (path ++ DirNum 1 :: nil) e1)
           (simplifyRec fullscp eRoot (path ++ DirNum 2 :: nil) e2)) eqn: Hsr.
    { destruct (simplifyRec fullscp eRoot (path ++ DirNum 1 :: nil) e1) eqn: Hsr1;
        try solve [simpl in Hsr; congruence].
      unfold listOptPlus. apply simplifyCurrent_correct; auto. }
    { destruct (simplifyRec fullscp eRoot (path ++ DirNum 1 :: nil) e1) eqn: Hsr1.
      { unfold listOptPlus in *. rewrite <- Hsr. apply IHe2.
        - rewrite focusIsAtPath_app. rewrite Hfi. rewrite Hff. reflexivity.
        - rewrite findFocusAtPath_app; auto. rewrite Hff. reflexivity. }
      { unfold listOptPlus in *. rewrite Hsr in Hsr1. rewrite <- Hsr1. apply IHe1.
        - rewrite focusIsAtPath_app. rewrite Hfi. rewrite Hff. reflexivity.
        - rewrite findFocusAtPath_app; auto. rewrite Hff. reflexivity. }
    }
  - simpl. intros. rewrite Forall_forall in *.
    rewrite listOptPlus_nil_r. 
    assert (Hinner: forall args1 pos, 
      pos <= List.length args ->
      args1 = skipn pos args ->
      fst (rewriteSteps defs eRoot (
      (fix F (pos : nat) (args0 : list Exp) {struct args0} : list Step :=
        match args0 with
        | nil => nil
        | arg :: args1 =>
            listOptPlus (simplifyRec fullscp eRoot (path ++ DirNum pos :: nil) arg)
              (F (S pos) args1)
        end) (S pos) args1)) = true).
    { clear Hdefs. induction args1; auto.
        intros. 
        destruct (simplifyRec fullscp eRoot (path ++ DirNum (S pos) :: nil) a) eqn: Hsr.
        - simpl in *. 
          apply IHargs1 with (pos:=S pos); auto.
          + rewrite <- firstn_skipn with (n:=pos)(l:=args). rewrite <- H1.
            rewrite app_length.
            simpl. rewrite <- plus_n_Sm. apply le_n_S.
            rewrite length_firstn.
            apply Min.min_case_strong; auto with arith.
          + rewrite skipn_S. rewrite <- H1. reflexivity.
        - unfold listOptPlus. rewrite <- Hsr.
          apply H with (x:=a)(path:=path ++ DirNum (S pos)::nil); try congruence; auto.
          + rewrite <- firstn_skipn with (n:=pos). rewrite in_app_iff.
            rewrite <- H1. simpl. right. left. reflexivity.
          + rewrite focusIsAtPath_app. rewrite Hfi. rewrite Hff.
            cbv -[le_lt_dec List.length]. 
            destruct (le_lt_dec (S pos) (List.length args)) as [Hle | Hgt]; auto.
            contradict Hgt.
            rewrite <- firstn_skipn with (n:=pos)(l:=args).
            rewrite app_length. rewrite <- H1. simpl.
            rewrite <- plus_n_Sm. apply le_not_lt. apply le_n_S.
            rewrite length_firstn.
            apply Min.min_case_strong; auto with arith.
          + rewrite findFocusAtPath_app; auto. rewrite Hff. 
            unfold findFocusAtPath. unfold findFocusAtDirection. 
            cbv -[getFunArg].
            rewrite skipn_getFunArg with (arg:=a)(args1:=args1); auto.
    }
    apply Hinner with (args1:=args)(pos:=0); auto.
    auto with arith.
Qed.

(* ***** *)

(*
Lemma focusIsAtPath_rewriteSteps_simplifyRec: forall defs fullscp eRoot path e,
  focusIsAtPath path eRoot = true ->
  findFocusAtPath path eRoot = e ->
  focusIsAtPath path (snd (rewriteSteps defs eRoot (simplifyRec fullscp eRoot path e))) = true.
Proof.
  admit.
Qed.
*)

Lemma simplifyHelper_correct: forall defs fullscp fuel eRoot ostepsexp,
  defsSubset axioms defs = true ->
  match ostepsexp with
  | None => True
  | Some (steps, e) => rewriteSteps defs eRoot steps = (true, e) 
  end ->    
  fst (rewriteSteps defs eRoot (simplifyHelper defs fullscp fuel ostepsexp)) = true. 
Proof.
  induction fuel.
  - intros ? ? Hdefs Hop. simpl. destruct ostepsexp as [[steps e]|]; auto.
  - intros ? ? Hdefs Hop. simpl. destruct ostepsexp as [[steps e]|]; auto.
    rewrite rewriteSteps_app; auto.
    2: rewrite Hop; reflexivity.
    rewrite Hop. simpl. apply IHfuel; auto.
    unfold simplifyTop.
    destruct (simplifyRec fullscp e nil e) eqn: Hsr.
    + unfold rewriteOptSteps. constructor.
    + unfold rewriteOptSteps. rewrite <- Hsr. 
      rewrite <- simplifyRec_correct with (defs:=defs)(fullscp:=fullscp)
          (eRoot:=e)(path:=nil)(e:=e); auto.
      apply surjective_pairing. 
Qed.

Theorem simplify_correct: forall defs fullscp fuel eRoot,
  defsSubset axioms defs = true ->
  fst (rewriteSteps defs eRoot (simplify defs fullscp fuel eRoot)) = true.
Proof.
  intros. unfold simplify.
  apply simplifyHelper_correct with (fuel:=fuel); auto.
  unfold simplifyTop.
  destruct (simplifyRec fullscp eRoot nil eRoot) eqn: Hsr.
  - simpl. constructor.
  - unfold rewriteOptSteps. 
    rewrite <- Hsr. rewrite <- simplifyRec_correct with (defs:=defs)(fullscp:=fullscp)
          (eRoot:=eRoot)(path:=nil)(e:=eRoot); auto.
    apply surjective_pairing.
Qed.

(* Print Assumptions simplify_correct. *)

(* ***** *)

Lemma lookup_name_eq: forall defs name1 name2 kind formals body,
  lookup name1 defs = Some (De kind name2 formals body) -> name1 = name2.
Proof.
  induction defs; simpl; try congruence.
  intros. destruct a as [kind' name' formals' body']. 
  destruct (string_dec name1 name') as [Heq | Hneq].
  - inversion H; subst. reflexivity.
  - apply IHdefs with (kind:=kind)(formals:=formals)(body:=body). assumption.
Qed.
      
Lemma lookup_In: forall defs name def, lookup name defs = Some def -> In def defs.
Proof.
  induction defs; simpl; try congruence.
  intros. destruct a as [kind' name' formals' body'].
  destruct (string_dec name name') as [Heq | Hneq].
  - inversion H; subst. left. reflexivity.
  - right. eapply IHdefs. eassumption.
Qed.

Lemma map_subVar_sameVars: forall vars es, NoDup vars -> List.length vars = List.length es ->
  map (fun x => subVar vars es x) vars = es.
Proof.
  induction vars as [|var vars]; destruct es as [|e es]; simpl; try congruence; auto.
  intros. destruct (string_dec var var); try congruence.
  f_equal. inversion H. subst. rewrite <- IHvars; auto. 
  apply map_In_ext.
  intros x Hin. destruct (string_dec x var); auto.
  subst. contradiction.
Qed.

Lemma unfoldSteps_correct: forall defs eRoot path e,
  Forall (fun def => match def with De kind name formals body => NoDup formals end) defs ->
  focusIsAtPath path eRoot = true ->
  findFocusAtPath path eRoot = e ->
  forall step, In step (unfoldSteps defs path e) ->
    fst (rewriteStep defs eRoot step) = true.
Proof.
  intros ? ? ? ? Hdefs Hfi Hff. revert path Hfi Hff. 
  induction e using Exp_fullInd; try solve [simpl; intuition].
  - simpl. intros. repeat (rewrite in_app_iff in * ).
    destruct H as [Hin | [Hin | Hin]].
    + apply IHe1 with (path:=path ++ DirQ::nil); auto.
      * rewrite focusIsAtPath_app. rewrite Hfi. rewrite Hff. reflexivity.
      * rewrite findFocusAtPath_app; auto. rewrite Hff. reflexivity.
    + apply IHe2 with (path:=path ++ DirA::nil); auto.
      * rewrite focusIsAtPath_app. rewrite Hfi. rewrite Hff. reflexivity.
      * rewrite findFocusAtPath_app; auto. rewrite Hff. reflexivity.
    + apply IHe3 with (path:=path ++ DirE::nil); auto.
      * rewrite focusIsAtPath_app. rewrite Hfi. rewrite Hff. reflexivity.
      * rewrite findFocusAtPath_app; auto. rewrite Hff. reflexivity.
  - simpl. intros.
    apply IHe with (path:=path ++ DirNum 1::nil); auto.
    + rewrite focusIsAtPath_app. rewrite Hfi. rewrite Hff. reflexivity.
    + rewrite findFocusAtPath_app; auto. rewrite Hff. reflexivity.
  - simpl. intros. rewrite in_app_iff in *. destruct H as [Hin | Hin].
    + apply IHe1 with (path:=path ++ DirNum 1::nil); auto.
      * rewrite focusIsAtPath_app. rewrite Hfi. rewrite Hff. reflexivity.
      * rewrite findFocusAtPath_app; auto. rewrite Hff. reflexivity.
    + apply IHe2 with (path:=path ++ DirNum 2::nil); auto.
      * rewrite focusIsAtPath_app. rewrite Hfi. rewrite Hff. reflexivity.
      * rewrite findFocusAtPath_app; auto. rewrite Hff. reflexivity.
  - simpl. intros. rewrite in_app_iff in *. destruct H0 as [Hin | Hin].
    { destruct (lookup fname defs) eqn: Heq; auto. destruct d; auto. destruct kind; auto.
      destruct (eq_nat_dec (List.length args) (List.length formals)); auto.
      simpl in Hin. destruct Hin; auto. subst.
      unfold rewriteStep. simpl. rewrite Heq.
      unfold equalityPath. simpl. rewrite Hfi. rewrite Hff.
      unfold equality. rewrite map_map.
      rewrite (lookup_name_eq _ _ Heq).
      unfold subE. fold subE.
      rewrite map_subVar_sameVars; auto.
      { rewrite expEqb_refl.
        apply focusIsAtPath_true_rewriteFocusAtPath_true; auto. }
      { rewrite Forall_forall in *. apply lookup_In in Heq.
        specialize (Hdefs _ Heq). simpl in *. assumption. }
    }
    { clear Hdefs. rewrite Forall_forall in *.
      assert (Hinner: forall args1 pos step,
        pos <= List.length args ->
        args1 = skipn pos args ->
        In step
        ((fix F (pos : nat) (args : list Exp) {struct args} : list Step :=
            match args with
            | nil => nil
            | arg :: args0 =>
                unfoldSteps defs (path ++ DirNum pos :: nil) arg ++
                F (S pos) args0
            end) (S pos) args1) ->
        fst (rewriteStep defs eRoot step) = true).
      { clear step Hin. induction args1 as [|arg args1]; auto.
        intros. rewrite in_app_iff in *. destruct H2 as [Hin | Hin].
        - apply H with (x:=arg)(path:=path ++ DirNum (S pos)::nil); auto.
          + rewrite <- firstn_skipn with (n:=pos)(l:=args).
            rewrite <- H1. rewrite in_app_iff. right. simpl. left. reflexivity.
          + rewrite focusIsAtPath_app. rewrite Hfi. rewrite Hff. 
            cbv [andb]. unfold focusIsAtPath. unfold focusIsAtDirection.
            destruct (le_lt_dec (S pos) (List.length args)) as [Hle | Hgt]; auto.
            contradict Hgt.
            rewrite <- firstn_skipn with (n:=pos)(l:=args).
            rewrite app_length. rewrite <- H1. simpl.
            rewrite <- plus_n_Sm. apply le_not_lt. apply le_n_S.
            rewrite length_firstn.
            apply Min.min_case_strong; auto with arith.
          + rewrite findFocusAtPath_app; auto. rewrite Hff. 
            unfold findFocusAtPath. unfold findFocusAtDirection. 
            cbv -[getFunArg].
            rewrite skipn_getFunArg with (arg:=arg)(args1:=args1); auto.
        - apply IHargs1 with (pos:= S pos); auto.
          + rewrite <- firstn_skipn with (n:=pos)(l:=args). rewrite <- H1.
            rewrite app_length.
            simpl. rewrite <- plus_n_Sm. apply le_n_S.
            rewrite length_firstn.
            apply Min.min_case_strong; auto with arith.
          + rewrite skipn_S. rewrite <- H1. reflexivity.
      }
      apply Hinner with (args1:=args)(pos:=0); auto. auto with arith.
    }
Qed.

(* Print Assumptions unfoldSteps_correct. *)

(* ***** *)

Lemma bestUnfoldStepRec_In: forall defs fullscp simplfuel steps e step,
  In (bestUnfoldStepRec defs fullscp simplfuel e
      (step, expSize (applySimplify defs fullscp simplfuel (snd (rewriteStep defs e step)))) 
      steps)
     (step::steps).
Proof.
  induction steps as [|step' steps].
  - simpl. intros. left. reflexivity.
  - unfold bestUnfoldStepRec. fold bestUnfoldStepRec. intros. unfold bestUnfoldStepOfPair.
    match goal with [|- context[if ?X then _ else _]] => destruct X as [Hle | Hlt] end.
    + simpl. right. apply IHsteps.
    + specialize (IHsteps e step). destruct IHsteps as [Heq | Hin].
      * simpl. left. assumption.
      * simpl. right. right. assumption.
Qed.

Lemma bestUnfoldStepAmong_In: forall defs fullscp simplfuel e steps step,
  bestUnfoldStepAmong defs fullscp simplfuel e steps = Some step ->
  In step steps.
Proof.
  unfold bestUnfoldStepAmong. intros. destruct steps; try congruence. 
  injection H. clear H. intro H. subst.
  apply bestUnfoldStepRec_In.  
Qed.

Lemma bestUnfoldStep_correct: forall defs fullscp simplfuel e step, 
  Forall (fun def => match def with De kind name formals body => NoDup formals end) defs ->
  bestUnfoldStep defs fullscp simplfuel e = Some step ->
  fst (rewriteStep defs e step) = true.
Proof.
  unfold bestUnfoldStep. intros.
  apply unfoldSteps_correct with (path:=nil)(e:=e); auto.
  eapply bestUnfoldStepAmong_In; eauto.
Qed.

Lemma chooseUnfoldStep_correct: forall defs fullscp whitelist blacklist simplfuel e step, 
  Forall (fun def => match def with De kind name formals body => NoDup formals end) defs ->
  chooseUnfoldStep defs fullscp whitelist blacklist simplfuel e = Some step ->
  fst (rewriteStep defs e step) = true.
Proof.
  unfold chooseUnfoldStep. intros.
  eapply bestUnfoldStep_correct; eauto.
Qed.

(* ***** *)

Lemma scpStepsRec_correct: forall defs fullscp unfoldfuel whitelist blacklist simplfuel e,
  defsSubset axioms defs = true ->
  Forall (fun def => match def with De kind name formals body => NoDup formals end) defs ->
fst
  (rewriteSteps defs e
     (scpStepsRec defs fullscp unfoldfuel whitelist blacklist simplfuel
        (optunfoldWithPrevstepsResult defs fullscp whitelist blacklist
           simplfuel
           (stepsWithResult defs (expWithSimplsteps defs fullscp simplfuel e))))) =
true.
Proof.
  induction unfoldfuel.
  - intros. simpl. apply simplify_correct. assumption.
  - intros. simpl.
    match goal with [|- context[match ?X with Some _ => _ | _ => _ end]]
      => destruct X eqn: Heq
    end.
    + rewrite rewriteSteps_app. 2: apply simplify_correct; auto.
      match goal with [|- context[s::?X]] => change (s::X) with ((s::nil) ++ X) end.
      rewrite rewriteSteps_app.
      2: simpl. 2: rewrite ifSame. 2: eapply chooseUnfoldStep_correct; eauto.
      simpl. rewrite ifSame. apply IHunfoldfuel; auto.
    + apply simplify_correct; auto.
Qed.

Theorem scpSteps_correct: forall defs fullscp unfoldfuel whitelist blacklist simplfuel e,
  defsSubset axioms defs = true ->
  Forall (fun def => match def with De kind name formals body => NoDup formals end) defs ->
  fst (rewriteSteps defs e (scpSteps defs fullscp unfoldfuel whitelist blacklist simplfuel e))
  = true.
Proof.
  intros. unfold scpSteps. apply scpStepsRec_correct; auto.
Qed.

(* Print Assumptions scpSteps_correct. *)
