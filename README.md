# A J-Bob Supercompiler #

## Overview ##

This repository contains a supercompiler for producing J-Bob proofs. There is also a (partial) J-Bob proof of the supercompiler correctness, created with the help of the supercompiler itself.

## How to try it? ##

### Obtaining the sources ###

Currently the only way to use the system is by obtaining the sources.
Simply clone this git repository.
The sources of J-Bob itself are needed as well and they are included as a git submodule.
So, after cloning the repository, get the J-Bob submodule as well:

    $ cd jbobscp
    $ git submodule init
    $ git submodule update

### Running inside DrRacket ###

The sources are written in Scheme, but were only tested with Racket 6.4.
So, you can use any other Scheme implementation, but at your own risk.
If you want to run them inside DrRacket, do not forget to follow the same instructions
as for J-Bob - after loading the source file in DrRacket:

* select Language R5RS
* uncheck the option "Disallow redefinition of initial bindings"
* for optimal performance, it is also recommended to select "No debugging or profiling"

### Files to run ###

Currently there are 2 files ready to be run directly:

* `src/Scheme/examples/j-bob-scp-examples.scm` contains some simple examples of J-Bob proofs
  using the supercompiler
* `src/Scheme/j-bob-scp-proofs.scm` contains the J-Bob proof of supercompiler correctness

As the proofs in both files are complete, if you run them unmodified the system will just
display some diagnostic messages and finally reply with `'t` (meaning all proofs are checked
successfully).
One way to see a J-Bob proof in action is:

* comment all lines of the proof
* start uncommenting lines one by one from the top and run the program at each step

In this case the system will display - at each step - the goal remaining to be proved.