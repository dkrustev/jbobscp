\documentclass{beamer}
\usetheme{Boadilla}
\usepackage{listings}

\lstset{
	language=Lisp,
	basicstyle=\normalsize,
	%	frame=tb,
	frame=none,
	breaklines=true,
	keywords={defun, dethm, if, atom, car, cdr, cons, equal, size, natp},
	escapeinside=||,
	showlines=true
}


% % % % % % % %

\title[A Supercompiler Assisting Its Verification]
  {A Supercompiler Assisting Its Own Formal Verification}
\author{Dimitur Krustev}

\pgfdeclareimage[height=0.7cm]{mylogo1}{IgeXaoLogo.jpg}

\institute[IGE+XAO Balkan]
{
	IGE+XAO Balkan \\ \vspace{0.3cm} \pgfuseimage{mylogo1}
}

\date[META 2016]{28 June 2016 / META 2016}

\begin{document}
	
\begin{frame}
	\maketitle
\end{frame}

\begin{frame}
	\tableofcontents
\end{frame}

% % % % % % % %

\section{Introduction}

\subsection{Goals}

\begin{frame}
	\frametitle{Supercompiler as a Tool for Its Own Verification}
	\begin{itemize}
		\item Supercompilers -- different applications:
		  \begin{itemize}
		  	\item program optimization
		  	\item program analysis
		  	\item program verification (and theorem proving in general)
		  	  \begin{itemize}
		  	  	\item relatively fewer practical examples
		  	  \end{itemize}
		  \end{itemize}
		\pause
		\item A non-trivial example of program verification: the correctness
		  proof of the supercompiler itself 
		  \begin{itemize}
		  	\item How much help can the supercompiler provide for this task?
		  \end{itemize}
    	\pause
	  	\item This work: partial assistance (not full self-verification)
	  	  \begin{itemize}
	  	  	\item global organization of supercompiler correctness proof
	  	  	  still done by hand (formulating suitable lemmas, selecting where
	  	  	  and what kind of induction to use, ...)
	  	  	\item much of the rest (low-level, boring technical details) --
	  	  	  handled automatically by the supercompiler itself
	  	  \end{itemize}
	  	\pause
	  	\item Important feature: the supercompiler can be called \emph{interactively} many times
	  	  during the development of the formal proof
	  	  \begin{itemize}
	  	  	\item unlike most supercompilers, which are all-or-nothing black boxes
	  	  	\item this idea may also be useful in other applications of supercompilation
	  	  \end{itemize}
	\end{itemize}
\end{frame}

\subsection{Approach}

\begin{frame}
	\frametitle{Approach}
	\begin{itemize}
		\item Choose a suitable proof assistant -- JBob:
		  \begin{itemize}
		  	\item built-in programming language (first-order functional)
		  	\item relatively weak logic, dealing mostly with decidable properties
		  	  (so proofs are computational by nature)
		  	\item proofs are program transformations (!!!)
		  \end{itemize}
		\pause
		\item Proof-generating (or ``certifying'') supercompiler
		  \begin{itemize}
		  	\item not a new idea -- Klyuchnikov and Romanenko, PSI 2014:
		  	  \begin{itemize}
		  	  	\item $ \mathit{scp}(\mathit{prg}) = (\mathit{prg}', \mathit{prf}) $
		  	  	\item $\mathit{prf} : \mathit{prg} \sim \mathit{prg}' $
		  	  \end{itemize}
%		  	 -- supercompiler
%		  	  returns a transformed program and a proof that it is equivalent to the input one
		  	\item here, even simpler: 
		  	  \begin{itemize}
		  	  	\item $\mathit{scp}(\mathit{prg}) = \mathit{prf}$
		  	  	\item $\mathit{rewrite}(\mathit{prg}, \mathit{prf}) = \mathit{prg}'$ ($\mathit{rewrite}$ is part of JBob itself)
		  	  \end{itemize}
%		  	  the supercompiler returns only the proof (as a sequence
%		  	  of program transformation steps); then we can obtain the resulting program by applying
%		  	  the steps (by an independent tool, a part of JBob)
		  \end{itemize}
		\pause
		\item Self-applicable supercompiler
		  \begin{itemize}
		  	\item written in the same language it can process
		  	\item can generate proofs about properties of its own source code
		  	\item but no Futamura projections (because currently the supercompiler is specialized
		  	  for proof generation, not program optimization)
		  \end{itemize}
	\end{itemize}
\end{frame}

\subsection{JBob By Example}

\begin{frame}[fragile]
	\frametitle{What Is JBob?}
	\begin{itemize}
		\item a simple proof assistant ($<$ 1 KLOC)
		\item from the book ``The Little Prover'' (Friedman, Eastlund)
		\item mostly for educational purposes
		\item programs in a first-order tiny subset of Lisp
		  \begin{itemize}
		  	\item only natural numbers, symbols, and cons-pairs
		  	\item only direct recursion
		  	\item always with a proof of termination
		  \end{itemize}
		\item theorems about properties of programs
		  \begin{itemize}
		  	\item theorem statement -- just a Boolean expression
		  	\item the \lstinline|(if q a e)| built-in operator 
		  	  serves double duty also as a logical connective 
		  	  ($\wedge$, $\vee$, $\neg$, $\rightarrow$ easily expressible,
		  	  e.g. $a \rightarrow b \equiv$ \lstinline|(if| $a$ $b$ \lstinline|'t)|)
		  	\item user-supplied detailed proof ...
		  	\item ... whose validity is checked by JBob
		  	\item proof aims to convert the theorem statement to \lstinline|'t|
		  	\item $\Rightarrow$ proof steps are program transformations 
		  	  (each step converts a designated subexpression to an equivalent one)
		  \end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{A JBob Program -- List Append}
\begin{lstlisting}
(
  (defun append (xs ys)
    (if (atom xs) ys
      (cons (car xs) (append (cdr xs) ys))))
|\pause|      
  ;; termination measure:
  (size xs)
|\pause|      
  ;; termination proof:
  ((Q) (natp/size xs))
  (() (if-true (if (atom xs) 't (< (size (cdr xs)) (size xs))) 'nil))
  ((E) (size/cdr xs))
  (() (if-same (atom xs) 't))
)    
\end{lstlisting}	
\end{frame}

\begin{frame}[fragile]
	\frametitle{A JBob Proof -- List Append Associativity}
	
\begin{tabular}{p{0.99\linewidth}}
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
((dethm append-assoc (xs ys zs)
  (equal (append (append xs ys) zs) (append xs (append ys zs))))
  (list-induction xs) 
\end{lstlisting}}\mbox{}\box0 \\ 
\begin{onlyenv}<2->
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
  ((A 1 1) (append xs ys)) 
\end{lstlisting}}\mbox{}\box0 
\end{onlyenv} \\
\begin{onlyenv}<4->
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
  ((A 1 1) (if-nest-A (atom xs) ys (cons (car xs) 
\end{lstlisting}}\mbox{}\box0 
\end{onlyenv} \\
\begin{onlyenv}<4->
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
     (append (cdr xs) ys)))) 
\end{lstlisting}}\mbox{}\box0 
\end{onlyenv} \\
\begin{onlyenv}<6->
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
  ... (2 similar steps)
\end{lstlisting}}\mbox{}\box0 
\end{onlyenv} \\
\begin{onlyenv}<8->
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
  ((A) (equal-same (append ys zs))) 
\end{lstlisting}}\mbox{}\box0 
\end{onlyenv} \\
\begin{onlyenv}<10->
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
  ... (12 more steps)
\end{lstlisting}}\mbox{}\box0 
\end{onlyenv} \\
\begin{onlyenv}<12->
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
  (() (if-same (atom xs) 't))
\end{lstlisting}}\mbox{}\box0 
\end{onlyenv} \\
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
) 
\end{lstlisting}}\mbox{}\box0 \\ \hline
\begin{onlyenv}<1-2>
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
(if (atom xs) 
  (equal (append (append xs ys) zs) (append xs (append ys zs)))
  (if (equal (append (append (cdr xs) ys) zs) (append (cdr xs) (append ys zs))) 
    (equal (append (append xs ys) zs) (append xs (append ys zs))) 't))
\end{lstlisting}}\mbox{}\box0
\end{onlyenv}
\begin{onlyenv}<3-4>
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
(if (atom xs)
  (equal (append (if (atom xs) ys (cons (car xs) (append (cdr xs) ys))) zs) (append xs (append ys zs)))
  (if (equal (append (append (cdr xs) ys) zs) (append (cdr xs) (append ys zs))) 
    (equal (append (append xs ys) zs) (append xs (append ys zs))) 't))
\end{lstlisting}}\mbox{}\box0
\end{onlyenv}
\begin{onlyenv}<5-6>
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
(if (atom xs)
  (equal (append ys zs) (append xs (append ys zs)))
  (if (equal (append (append (cdr xs) ys) zs) (append (cdr xs) (append ys zs))) 
    (equal (append (append xs ys) zs) (append xs (append ys zs))) 't))

\end{lstlisting}}\mbox{}\box0
\end{onlyenv}
\begin{onlyenv}<7-8>
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
(if (atom xs) 
  (equal (append ys zs) (append ys zs)) 
  (if (equal (append (append (cdr xs) ys) zs) (append (cdr xs) (append ys zs))) 
    (equal (append (append xs ys) zs) (append xs (append ys zs))) 't))

\end{lstlisting}}\mbox{}\box0
\end{onlyenv}
\begin{onlyenv}<9-10>
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
(if (atom xs) 
  't 
  (if (equal (append (append (cdr xs) ys) zs) (append (cdr xs) (append ys zs))) 
    (equal (append (append xs ys) zs) (append xs (append ys zs))) 't))

\end{lstlisting}}\mbox{}\box0
\end{onlyenv}
\begin{onlyenv}<11-12>
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
(if (atom xs) 
  't 
  't
)



\end{lstlisting}}\mbox{}\box0
\end{onlyenv}
\begin{onlyenv}<13->
\setbox0=\hbox{\begin{lstlisting}[basicstyle=\small]
't 






\end{lstlisting}}\mbox{}\box0
\end{onlyenv}
\end{tabular}	

\end{frame}

% % % % % % % %

\section{Implementation}

\subsection{Architecture Overview}

\begin{frame}
	\frametitle{Architecture Overview}
	\begin{itemize}
  	  	\item $\mathit{scp}(\mathit{defs}, \mathit{exp}) = \mathit{prfsteps}$ 
  	  	  (and then use the JBob rewriter to produce result expression: 
  	  	  $\mathit{rewrite}(\mathit{defs}, \mathit{exp}, \mathit{prfsteps}) = \mathit{exp}'$)
  	  	\item $\mathit{scp}$ produces 2 kinds of steps:
  	  	  \begin{itemize}
  	  	  	\item basic transformations (``obvious'' redexes such as \lstinline|(car (cons ...))|
  	  	  	  , built-in function called with known arguments, positive/negative information propagation, \ldots)
  	  	  	\item function call unfolding
  	  	  \end{itemize}
  	  	\item these 2 kinds of steps produced by 2 different (interleaved) phases:
  	  	  \begin{itemize}
  	  	  	\item $\mathit{scp} = (\mathit{simplify})* \cdot (\mathit{unfold} \cdot (\mathit{simplify})*)*$
  	  	  \end{itemize}
  	  	\pause
  	  	\item What about ... 
  	  	  \begin{itemize}
  	  	  	\item folding? 
  	  	  	  \begin{itemize}
  	  	  	  	\item not obvious how to integrate inside a JBob proof 
  	  	  	  	\item implementation and correctness proof simplified by lack of folding
  	  	  	  \end{itemize}
  	  	  	\pause
  	  	  	\item whistle? Just 2 counters limiting number of steps produced by each phase
  	  	  	\pause
  	  	  	\item generalization?
  	  	  	  \begin{itemize}
  	  	  	  	\item choice of call to unfold not predetermined (as all functions terminate)
  	  	  	  	\item provide different unfolding strategies (best, CBN, CBV) as a form of generalization
  	  	  	  \end{itemize}
  	  	  \end{itemize}
	\end{itemize}
\end{frame}

\subsection{Basic Transformations}

\begin{frame}[fragile]
	\frametitle{PE-like Transformation Steps}
\begin{table}
\begin{tabular}{p{0.20\linewidth} | p{0.35\linewidth} | p{0.30\linewidth}}
	Name  & Original expression & Result expression 
	\\ \hline
	\lstinline|atom/cons|	
	&  
	\lstinline|(atom (cons x y))|
	&  
	\lstinline|'nil|
	\\ \hline
	\lstinline|car/cons|	
	&  
	\lstinline|(car (cons x y))|
	&  
	\lstinline|x|
	\\ \hline
	\lstinline|cdr/cons|	
	&  
	\lstinline|(cdr (cons x y))|
	&  
	\lstinline|y|
	\\ \hline
	\lstinline|if-true|	
	&  
	\lstinline|(if 't x y)|
	&  
	\lstinline|x|
	\\ \hline
	\lstinline|if-false|	
	&  
	\lstinline|(if 'nil x y)|
	&  
	\lstinline|y|
	\\ \hline
\end{tabular} 
\end{table}	

\begin{itemize}
	\item + evaluation of built-in functions with known arguments
\end{itemize}
	
\end{frame}

\begin{frame}[fragile]
	\frametitle{Information Propagation Steps}
\begin{tabular}{p{0.20\linewidth} | p{0.35\linewidth} | p{0.30\linewidth}}
	Name  & Original expression & Result expression 
	\\ \hline
	\lstinline|if-nest-A|	
	&  
	\begin{lstlisting}[aboveskip=-0.5\baselineskip, belowskip=-\baselineskip]
(if x
  (... (if x y z) ...)
  ...
)
	\end{lstlisting}
	&  
	\begin{lstlisting}[aboveskip=-0.5\baselineskip, belowskip=-\baselineskip]
(if x
  (... y ...)
  ...
)
	\end{lstlisting}
	\\ \hline
	\lstinline|if-nest-E|	
	&  
	\begin{lstlisting}[aboveskip=-0.5\baselineskip, belowskip=-\baselineskip]
(if x
  ...
  (... (if x y z) ...)
)
	\end{lstlisting}
	&  
	\begin{lstlisting}[aboveskip=-0.5\baselineskip, belowskip=-\baselineskip]
(if x
  ...
  (... z ...)
)
	\end{lstlisting}
	\\ \hline
	\lstinline|if-same|	
	&  
	\lstinline|(if x y y)|
	&  
	\lstinline|y|
	\\ \hline
	\lstinline|equal-same|	
	&  
	\lstinline|(equal x x)|
	&  
	\lstinline|'t|  \\ \hline
\end{tabular} 
	
\end{frame}

\begin{frame}
	\frametitle{If Lifting}
	\begin{itemize}
		\item Supercompilers also perform ``if-lifting'':
		  \lstinline|(if (if q a e) a' e')| $\Rightarrow$
		  \lstinline|(if q (if a a' e') (if e a' e'))|
		\item no direct JBob rule, but derivable anyway:
	\end{itemize}
\begin{table}
\begin{tabular}{ll}
	& \lstinline|(if (if q a e) a' e')| \\
	= & ~\{\texttt{if-same}, right-to-left\} \\
	& \lstinline|(if q (if (if q a e) a' e') (if (if q a e) a' e'))| \\
	= & ~\{\texttt{if-nest-A}, left-to-right\} \\
	& \lstinline|(if q (if a a' e') (if (if q a e) a' e'))|  \\
	= & ~\{\texttt{if-nest-E}, left-to-right\} \\
	& \lstinline|(if q (if a a' e') (if e a' e'))|
\end{tabular}
\end{table}
	\begin{itemize}
		\item if-lifting also works inside function calls:
		  \lstinline|(f x (if q a e) y)| $\Rightarrow$
		  \lstinline|(if q (f x a y) (f x e y))|
		  \begin{itemize}
		  	\item not safe in general
		  	\item safe here, because of termination guarantee
		  \end{itemize}
	\end{itemize}
\end{frame}

\subsection{Supercompiler-assisted Proofs}

\begin{frame}
	\frametitle{JBob Integration}
	\begin{itemize}
		\item new kinds of steps possible inside a JBob proof:
		  \begin{itemize}
		  	\item \lstinline|(scp ...)| -- call supercompiler (with different options)
		  	  on current proof goal
		  	\item \lstinline|(simpl ...)| -- the same, but generate only PE-like basic transformation step
		  	  (optimization, sometimes produces fewer proof steps, but less powerful)
		  	\item \ldots (some other non-supercompiler-related extensions)
		  \end{itemize}
		\item ``proof expander'' converts a proof with extended set of steps into a 
		  proof with only standard JBob steps
		\item resulting expanded proof still checked by (unmodified) JBob
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Example Proof with Supercompilation}
	\begin{itemize}
		\item The same proof of list append associativity, with the help of
		  the supercompiler:
\begin{lstlisting}
((dethm append-assoc (xs ys zs)
  (equal (append (append xs ys) zs) (append xs (append ys zs))))
  (list-induction xs)
  (scp 5 50)
  (expand ((append 5)) (equal-if (append (append (cdr xs) ys) zs) (append (cdr xs) (append ys zs))))
  (scp 0 50)
)
\end{lstlisting}
        \item only 3 steps
        \item expand to the same 18 steps as in the manual proof (in slightly different order)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Supercompiler Correctness Proof}
	\begin{itemize}
		\item Recall: $\mathit{scp}(\mathit{defs}, \mathit{exp}) = \mathit{prfsteps}$ 
		  and then $\mathit{rewrite}(\mathit{defs}, \mathit{exp}, \mathit{prfsteps}) = \mathit{exp}'$
		\item $\mathit{rewrite}$ is part of JBob -- we assume it is correct
		\item What remains to prove is that the sequence of transformation steps
		  returned by $\mathit{scp}$ is correct
		\item each step in the sequence is correct by definition (comes from JBob);
		  we only need to ensure that all steps in the sequence are applicable
		\item for clarity, use a slightly modified version of $\mathit{rewrite}$:
		  $\mathit{rewrite}'(\mathit{defs}, \mathit{exp}, \mathit{prfsteps}) = (b,\mathit{exp}')$,
		  where $b$ = \lstinline|'t| iff the application of $\mathit{prfsteps}$ did not get stuck
		\item the supercompiler correctness condition becomes:
		  $\mathit{fst}(\mathit{rewrite}'(\mathit{defs}, e, \mathit{scp}(\mathit{defs}, e))) = $ \lstinline|'t|
		\item correctness proof in JBob currently incomplete (due to some performance issues and lack of time);
		  a parallel proof in Coq shows there are no obstacle in principle to complete the proof
	\end{itemize}
\end{frame}

% % % % % % % %

\section{Analysis and Conclusions}

\subsection{Performance Evaluation}

\begin{frame}
	\frametitle{Performance Evaluation}
	\begin{itemize}
		\item Statistics based on the existing part of the supercompiler correctness proof:
\begin{table}
\centering
\begin{tabular}{lrrrr}
	& Steps & Scp steps & Exp.steps & Ratio \\ \hline
	TOTAL & 219 & 128 & 9268 & 42.32 \\
	Typical proofs & 99 & 49 & 707 & 7.10 \\
	Proofs by computation & 6 & 6 & 1806 & 301.00 \\
	Mixed-style proofs & 114 & 73 & 6755 & 59.25
\end{tabular}		
\end{table}		
		  \begin{itemize}
		  	\item savings in number of proof steps: about 10-100 times
		  \end{itemize}
		\pause
		\item BUT
		  \begin{itemize}
		  	\item proof expansion takes $\sim$40 sec (on a typical laptop)
		  	\item with proof checking by JBob, total time becomes $\sim$75 sec
		  	\item not very practical for interactive proof creation
		  	  (mainly because all proofs must re-expanded and re-checked after
		  	  each interactive proof modification)
		  \end{itemize}  		  
	\end{itemize}
\end{frame}

\subsection{Future Work}

\begin{frame}
	\frametitle{Ideas for Future Work}
	\begin{itemize}
		\item Performance problems mostly due to JBob organization; important to avoid re-checking
		  all proofs in interactive mode:
		  \begin{itemize}
		  	\item create a dedicated JBob REPL
		  	\item or at least a stateful API to run inside Scheme REPL
		  \end{itemize}
		\item Another source of slow performance: JBob proof steps are too fine-grained;
		  even trivial proofs by computation may require many proof steps
		\item $\Rightarrow$ adapt the approach to other proof assistants, with
		  more powerful built-in inference steps
		\item In particular, a supercompiler-based reflection tactic for a more
		  popular proof assistant (like Coq or Agda) would be cool
		\item Adapt the idea of interactive supercompilation to other application areas
		  (program optimization, program analysis)
	\end{itemize}
\end{frame}

%\subsection{...}

%%%%%%%%%%

\section*{Summary}

\begin{frame}{Summary}
	
	% Keep the summary *very short*.
	\begin{itemize}
		\item a proof-producing supercompiler working together with a classical proof assistant (JBob)
		\item the supercompiler is also self-applicable
		\item $\Rightarrow$ successfully produces fragments of its own correctness proof
		\item the supercompiler is interactive -- can be called many times during a proof,
		  each time making some progress towards the final goal
		\item a supercompiler returning a list of transformation steps:
		  \begin{itemize}
		  	\item permits independent verification of each result of supercompilation
		  	\item simplifies supercompiler correctness proof
		  \end{itemize}
	\end{itemize}
	
	% The following outlook is optional.
	\vskip0pt plus.5fill
	\begin{itemize}
		\item
		Outlook
		\begin{itemize}
			\item improve system performance
			\item adapt the idea to other proof assistants
		\end{itemize}
	\end{itemize}
\end{frame}

\end{document}